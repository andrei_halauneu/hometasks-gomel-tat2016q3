Feature: Implement actions to purchase goods on Aliexpress, viz https://www.aliexpress.com/

Narrative:
As anonymous user
I want to implement a steps for shopping
So that i can emulate purchase actions

Scenario: Choose any category by navigation ;
Given On https://www.aliexpress.com/
When I pick item <category> and click search button
Then I get page <result> with items related to category

Examples:
|category    |result  |
|Bicycles    |Bicycles|

Scenario: Check product page of any item from Bicycles ;
Given On search result page of Bicycles category
When I click on any bicycle item
Then I get redirected to page with this item's description

Scenario: Perform search by a trademark ;
Given On search results page of Bicycles category
When I click on SAVA brand icon
Then I get only items of this brand on search page

Scenario: Add item to Cart ;
Given On items description page
When I click Add to Cart button of item
Then I get selected item in my Cart

Scenario: Remove item from Cart;
Given On Cart page
When I click Remove button of item
Then I get this item removed
package com.epam.tat.ht8.product.ali.steps;


import com.epam.tat.ht8.framework.logging.Log;
import com.epam.tat.ht8.framework.ui.browser.Browser;
import com.epam.tat.ht8.product.ali.screen.CartPage;
import com.epam.tat.ht8.product.ali.screen.CategoriesPage;
import com.epam.tat.ht8.product.ali.screen.ItemPage;
import com.epam.tat.ht8.product.ali.screen.bicycles.CyclesBrandPage;
import com.epam.tat.ht8.product.ali.service.ShopService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.testng.Assert;

/**
 * Created by Andrei Halauneu on 06.11.2016.
 */
public class AliSteps {
    private static final String URL = "https://www.aliexpress.com/";
    private static final String CATEGORY = "Bicycle";
    private CategoriesPage categoriesPage = new CategoriesPage();
    private CyclesBrandPage cyclesBrandPage = new CyclesBrandPage();
    private ShopService shopService = new ShopService();
    private ItemPage itemPage = new ItemPage();
    private CartPage cartPage = new CartPage();


    @Given("On https://www.aliexpress.com/")
    public void openAliPage() {
        Log.info("openUrl");
        Browser.getBrowser().open(URL);
    }

    @When("I pick item <category> and click search button")
    public void pickItemCategory(@Named("category") String category) {
        Log.info("pickItemCategory");
        shopService.pickSelection(category);
    }

    @Then("I get page <result> with items related to category")
    public void checkSearchResults(@Named("result") String result) {
        Log.info("checkResultOfSearchCategory");
        String actualCategory = categoriesPage.getCurrentCategory();
        String expectedCategory = result;
        Assert.assertEquals(actualCategory, expectedCategory, "checkResultOfSearchCategory error");
        Browser.getBrowser().close();
    }

    @Given("On search result page of Bicycles category")
    public void openBikePageResults() {
        Log.info("openBikePageResults");
        Browser.getBrowser().open(URL);
        shopService.pickSelection(CATEGORY);
    }

    @When("I click on any bicycle item")
    public void pickFirstItem() {
        Log.info("pickItem");
        shopService.pickFirstBike();
    }

    @Then("I get redirected to page with this item's description")
    public void checkNeededWatchesBrand() {
        Log.info("checkNeededWatchesBrand");
        Assert.assertTrue(cyclesBrandPage.isSavaSelected());
        Browser.getBrowser().close();
    }

    @Given("On search results page of Bicycles category")
    public void openBrandPageResults() {
        Log.info("openBikePageResults");
        Browser.getBrowser().open(URL);
        shopService.pickSelection(CATEGORY);
    }

    @When("I click on SAVA brand icon")
    public void pickSavaBike() {
        Log.info("pickBrand");
        shopService.chooseSavaTm();
    }

    @Then("I get only items of this brand on search page")
    public void checkForBrandSelection() {
        Log.info("checkForBrand");
        Assert.assertTrue(itemPage.isOnItemPage());
        Browser.getBrowser().close();
    }

    @Given("On items description page")
    public void openItemPage() {
        Browser.getBrowser().open(URL);
        shopService.pickSelection(CATEGORY);
        shopService.chooseSavaTm();
        shopService.pickFirstBike();
    }

    @When("I click Add to Cart button of item")
    public void additemToCart() {
        Log.info("addItemToCart");
        shopService.addItemToCart();
    }

    @Then("I get selected item in my Cart")
    public void checkItemInCart() {
        Log.info("checkItemInCart");
        Assert.assertTrue(itemPage.checkItemAddedToCart());
    }

    @Given("On Cart page")
    public void enterToCart() {
        Log.info("enterToCart");
        shopService.enterCart();
    }

    @When("I click Remove button of item")
    public void removeFromCart() {
        Log.info("removeFromCart");
        shopService.deleteItemFromCart();
    }

    @Then("I get this item removed")
    public void checkForRemoval() {
        Log.info("checkForRemoval");
        Assert.assertTrue(cartPage.removeFromCart());
        Browser.getBrowser().close();
    }
}

package com.epam.tat.ht8.test;

import com.epam.tat.ht8.framework.ui.browser.Browser;
import com.epam.tat.ht8.product.ali.screen.CartPage;
import com.epam.tat.ht8.product.ali.service.ShopService;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by Andrei Halauneu on 06.11.2016.
 */
public class AliStepsTest {
    private ShopService shopService = new ShopService();
    private CartPage cartPage = new CartPage();

    @BeforeMethod
    public void setUp() {
        Browser.getBrowser().open("https://www.aliexpress.com/");
    }

    @Test
    public void aliTest() {
        shopService.pickSelection("Bicycle");
        shopService.chooseSavaTm();
        shopService.pickFirstBike();
        shopService.addItemToCart();
        shopService.enterCart();
        shopService.deleteItemFromCart();
        cartPage.removeFromCart();
    }

    @AfterMethod
    public void close() {
        Browser.getBrowser().close();
    }
}
package com.epam.tat.ht8.framework.runner;

import com.epam.tat.ht8.product.ali.steps.AliSteps;
import org.jbehave.core.annotations.Configure;
import org.jbehave.core.annotations.UsingEmbedder;
import org.jbehave.core.annotations.UsingSteps;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.AnnotatedEmbedderRunner;
import org.jbehave.core.junit.JUnitStories;
import org.junit.runner.RunWith;

import java.util.List;

/**
 * Created by Andrei Halauneu on 06.11.2016.
 */
@RunWith(AnnotatedEmbedderRunner.class)
@Configure
@UsingEmbedder(embedder = Embedder.class, generateViewAfterStories = true, ignoreFailureInStories = true,
        ignoreFailureInView = false, stepsFactory = true)
@UsingSteps(instances = {AliSteps.class})
public class Runner extends JUnitStories {

    protected List<String> storyPaths() {
        return new StoryFinder().findPaths(CodeLocations.codeLocationFromPath("src/main/resources"),
                "stories/*.story", "");
    }

}

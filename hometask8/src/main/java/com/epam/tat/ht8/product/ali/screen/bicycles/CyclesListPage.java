package com.epam.tat.ht8.product.ali.screen.bicycles;


import com.epam.tat.ht8.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Andrei Halauneu on 06.11.2016.
 */
public class CyclesListPage {
    private static final By FIRST_ITEM = By.xpath("//h3[@class='icon-hotproduct']");
    private Element firstCycle = new Element(FIRST_ITEM);

    public CyclesListPage pickFirstItem() {
        firstCycle.click();
        return this;
    }
}

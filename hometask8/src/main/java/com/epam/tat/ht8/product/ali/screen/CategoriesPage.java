package com.epam.tat.ht8.product.ali.screen;

import com.epam.tat.ht8.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Andrei Halauneu on 06.11.2016.
 */
public class CategoriesPage {
    private static final By SEARCH_CATEGORY = By.xpath("//div[@id='aliGlobalCrumb']//h1//span");

    private Element searchResultCategory = new Element(SEARCH_CATEGORY);

    public String getCurrentCategory() {
        return searchResultCategory.getText();
    }
}

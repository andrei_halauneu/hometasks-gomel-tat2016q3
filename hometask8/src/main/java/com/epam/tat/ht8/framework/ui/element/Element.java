package com.epam.tat.ht8.framework.ui.element;

import com.epam.tat.ht8.framework.ui.browser.Browser;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.File;

/**
 * Created by Andrei Halauneu on 29.10.2016.
 */
public class Element {

    @Getter
    private By by;

    public Element(By by) {
        this.by = by;
    }

    public WebElement getWrappedWebElement() {
        return Browser.getBrowser().findElement(by);
    }

    public boolean isPresent() {
        return Browser.getBrowser().isElementPresent(by);
    }

    public boolean isVisible() {
        return Browser.getBrowser().isElementVisible(by);
    }

    public void waitForAppear() {
        Browser.getBrowser().waitForElementIsVisible(by);
    }

    public void waitForPresence() {
        Browser.getBrowser().waitForElementIsPresent(by);
    }

    public void waitForDisappear() {
        Browser.getBrowser().waitForElementIsInvisible(by);
    }

    public void switchToFrame() {
        Browser.getBrowser().switchToFrame(getWrappedWebElement());
    }

    public void switchToDefaultFrame() {
        Browser.getBrowser().switchToMainFrame();
    }

    public void sendFile(String path) {
        getWrappedWebElement().findElement(by).sendKeys(new File(path).getAbsolutePath());
    }

    public String getText() {
        return getWrappedWebElement().getText();
    }

    public void typeValue(String value) {
        WebElement webElement = getWrappedWebElement();
        webElement.clear();
        webElement.sendKeys(value);
    }

    public void click() {
        getWrappedWebElement().click();
    }

    public void submit(){
        getWrappedWebElement().submit();
    }

    public void selectFromDropDown(Element element, String selectValue) {
        Select select = new Select(element.getWrappedWebElement());
        select.selectByVisibleText(selectValue);
    }
}

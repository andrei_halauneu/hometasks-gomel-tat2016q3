package com.epam.tat.ht8.product.ali.service;


import com.epam.tat.ht8.product.ali.screen.CartPage;
import com.epam.tat.ht8.product.ali.screen.ItemPage;
import com.epam.tat.ht8.product.ali.screen.TopMenuPage;
import com.epam.tat.ht8.product.ali.screen.bicycles.CyclesBrandPage;
import com.epam.tat.ht8.product.ali.screen.bicycles.CyclesListPage;

/**
 * Created by Andrei Halauneu on 06.11.2016.
 */
public class ShopService {
    private TopMenuPage topMenuPage = new TopMenuPage();
    private CyclesBrandPage cyclesBrandPage = new CyclesBrandPage();
    private CyclesListPage cyclesListPage = new CyclesListPage();
    private ItemPage itemPage = new ItemPage();
    private CartPage cartPage = new CartPage();

    public void pickSelection(String category) {
        itemPage.closeWelcomePopup();
        topMenuPage.clickOnSearchSectionMenu().pickBikeCategory(category).clickSearchButton();
    }

    public void chooseSavaTm() {
        cyclesBrandPage.pickSavaCycle();
    }

    public void pickFirstBike() {
        cyclesListPage.pickFirstItem();
    }

    public void addItemToCart() {
        itemPage.selectType().addToCart().closePopupItem();
    }

    public void enterCart() {
        topMenuPage.clickOnCartSection();
    }

    public void deleteItemFromCart() {
        cartPage.removeItem();
    }
}

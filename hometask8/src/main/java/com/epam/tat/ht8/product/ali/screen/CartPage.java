package com.epam.tat.ht8.product.ali.screen;

import com.epam.tat.ht8.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Andrei Halauneu on 06.11.2016.
 */
public class CartPage {
    private static final By REMOVE_BUTTON = By.xpath("//a[@class='remove-single-product']");
    private static final By EMPTY_CART = By.xpath("//div[@id='notice']");

    private Element removeItemButton = new Element(REMOVE_BUTTON);
    private Element emptyCart = new Element(EMPTY_CART);

    public void removeItem() {
        removeItemButton.waitForAppear();
        removeItemButton.click();
    }

    public boolean removeFromCart() {
        emptyCart.waitForAppear();
        return true;
    }
}

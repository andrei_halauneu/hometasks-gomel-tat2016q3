package com.epam.tat.ht8.product.ali.screen;

import com.epam.tat.ht8.framework.ui.element.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;

/**
 * Created by Andrei Halauneu on 06.11.2016.
 */
public class TopMenuPage {
    private static final By SEARCH_BUTTON = By.xpath("//input[@class='search-button']");
    private static final By CATEGORIES_LIST = By.xpath("//select[@id='search-dropdown-box']");
    private static final By CATEGORIES_SEARCH_BUTTON = By.xpath("//div[@id='search-cate']");
    private static final By CART_ICON

            = By.xpath("//a[@rel='nofollow' and @href='//shoppingcart.aliexpress.com/shopcart/shopcartDetail.htm']");

    private Element selectCategory = new Element(CATEGORIES_LIST);
    private Element searchButtonCategory = new Element(SEARCH_BUTTON);
    private Element cartIcon = new Element(CART_ICON);
    private Element searchSection = new Element(CATEGORIES_SEARCH_BUTTON);

    public TopMenuPage clickOnSearchSectionMenu() {
        try {
            searchSection.waitForAppear();
            searchSection.click();

        } catch (WebDriverException e) {
            clickOnSearchSectionMenu();
        }
        return this;
    }

    public TopMenuPage pickBikeCategory(String category) {
        selectCategory.selectFromDropDown(selectCategory, category);
        return this;
    }

    public TopMenuPage clickSearchButton() {
        searchButtonCategory.click();
        return this;
    }

    public TopMenuPage clickOnCartSection() {
        cartIcon.waitForAppear();
        cartIcon.click();
        return this;
    }

}

package com.epam.tat.ht8.framework.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class FileDownloadValidator {
    private static final String DOWNLOADED_FILE_PATH = "./src/main/resources/downloads/%s.txt";
    private static final String DOWNLOADS_DIR = "./src/main/resources/downloads";
    private static final String FILE_PATH = "./src/main/resources/files/%s.txt";
    private static final String GENERATED_FILES_DIR = "./src/main/resources/files/";
    private static final int WAIT_FILE_DOWNLOAD_SECONDS = 10;
    private File fileDownloadContent;
    private File fileExistContent;

    public void waitForFile(String fileName) {
        fileDownloadContent = new File(String.format(DOWNLOADED_FILE_PATH, fileName));
        FileUtils.waitFor(fileDownloadContent, WAIT_FILE_DOWNLOAD_SECONDS);
    }

    public boolean isContentFileSame(String fileName) throws IOException {
        fileExistContent = new File(new File(String.format(FILE_PATH, fileName)).getAbsolutePath());
        return FileUtils.contentEquals(fileDownloadContent, fileExistContent);
    }

    public void deleteDownloadDirectory() throws IOException {
        FileUtils.deleteDirectory(new File(DOWNLOADS_DIR));
    }

    public void deleteGenerateFilesDirectory() throws IOException {
        FileUtils.deleteDirectory(new File(GENERATED_FILES_DIR));
    }
}


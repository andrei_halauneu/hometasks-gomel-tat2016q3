package com.epam.tat.ht8.product.ali.screen;


import com.epam.tat.ht8.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Andrei Halauneu on 06.11.2016.
 */
public class ItemPage {
    private static final By BIKE_OPTION = By.xpath("//a[@data-sku-id='10']/span");
    private static final By ADD_TO_CART_BUTTON = By.xpath("//a[@id='j-add-cart-btn']");
    private static final By CLOSE_POPUP_PURCHASE_BUTTON = By.xpath("//a[@class='ui-window-close']");
    private static final By CLOSE_WELCOME_POPUP_BUTTON = By.xpath("//a[@class='close-layer']");
    private static final By ITEM_ADDED_TO_CART_BUTTON = By.xpath("//h3[@class='ui-feedback-header']");

    private Element bikeType = new Element(BIKE_OPTION);
    private Element addToCartButton = new Element(ADD_TO_CART_BUTTON);
    private Element closePopupButton = new Element(CLOSE_POPUP_PURCHASE_BUTTON);
    private Element closeWelcomePopupButton = new Element(CLOSE_WELCOME_POPUP_BUTTON);
    private Element itemAddedToCart = new Element(ITEM_ADDED_TO_CART_BUTTON);

    public ItemPage selectType() {
        bikeType.click();
        return this;
    }

    public ItemPage addToCart() {
        addToCartButton.click();
        return this;
    }

    public boolean isOnItemPage() {
        return addToCartButton.isVisible();
    }

    public ItemPage closePopupItem() {
        closePopupButton.click();
        closePopupButton.waitForDisappear();
        return this;
    }

    public ItemPage closeWelcomePopup() {
        closeWelcomePopupButton.waitForAppear();
        closeWelcomePopupButton.click();
        return this;
    }

    public boolean checkItemAddedToCart() {
        boolean itemAddedInCart = itemAddedToCart.isVisible();
        closePopupItem();
        return itemAddedInCart;
    }
}

package com.epam.tat.ht8.product.ali.screen.bicycles;


import com.epam.tat.ht8.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Andrei Halauneu on 06.11.2016.
 */
public class CyclesBrandPage {
    private static final By SAVA_ICON = By.xpath("//a[@title='SAVA']");
    private static final By SAVA_SELECTED
            = By.xpath("//li[@class='brand-content  selected ']//a[@title='SAVA']");

    private Element pickSava = new Element(SAVA_ICON);
    private Element activeSAVA = new Element(SAVA_SELECTED);

    public CyclesBrandPage pickSavaCycle() {
        pickSava.click();
        return this;
    }

    public boolean isSavaSelected() {
        activeSAVA.waitForAppear();
        return activeSAVA.isVisible();
    }
}

package com.epam.tat.ht8.framework.util;

import lombok.Getter;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class FileGenerator {
    private static final int COUNT_FOR_CIRCLE = 10;
    private static final String FILES_PATH = "./src/main/resources/files";
    @Getter
    private String filePath;
    @Getter
    private String fileName;

    public void generateFile() throws IOException {
        filePath = "./src/main/resources/files/" + "%s.txt";
        fileName = Randoms.randomAlphabetic();
        Path path = Paths.get(String.format(filePath, fileName));
        Files.createDirectories(Paths.get(FILES_PATH));
        Files.createFile(path);
        writeTextInFile(path);
    }

    public Map<String, String> generateSeveralFiles(int countOfFiles) throws IOException {
        Map<String, String> filesList = new HashMap<>();
        for (int i = 0; i < countOfFiles; i++) {
            filePath = "./src/main/resources/files/" + "%s.txt";
            fileName = Randoms.randomAlphabetic();
            Path path = Paths.get(String.format(filePath, fileName));
            Files.createDirectories(Paths.get(FILES_PATH));
            Files.createFile(path);
            writeTextInFile(path);
            filesList.put(fileName, filePath);
        }
        return filesList;
    }

    public void writeTextInFile(Path path) throws IOException {
        try (BufferedWriter writer = Files.newBufferedWriter(path,
                StandardCharsets.UTF_8, StandardOpenOption.WRITE)) {
            for (int i = 0; i < COUNT_FOR_CIRCLE; i++) {
                writer.write(String.format(Randoms.randomAlphabetic() + "%n"));
            }
        } catch (IllegalArgumentException e) {
            System.err.println("Error generating file");
        }
    }
}

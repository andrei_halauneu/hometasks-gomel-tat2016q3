package com.epam.tat.ht8.framework.runner;

import org.jbehave.core.Embeddable;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.junit.Test;

import java.util.List;

/**
 * Created by Andrei Halauneu on 06.11.2016.
 */
public class StoriesRunner implements Embeddable {
    private Embedder embedder;

    @Override
    public void useEmbedder(Embedder param) {
        this.embedder = param;
    }

    @Test
    @Override
    public void run() throws Throwable {
        embedder.runStoriesAsPaths(storyPaths());
    }

    private List<String> storyPaths() {
        return new StoryFinder().findPaths(CodeLocations.codeLocationFromPath("src/main/resources"),
                "stories/*.story", "");
    }
}

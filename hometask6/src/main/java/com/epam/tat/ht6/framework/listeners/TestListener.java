package com.epam.tat.ht6.framework.listeners;

import com.epam.tat.ht6.framework.logging.Log;
import com.epam.tat.ht6.framework.ui.browser.Browser;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * Created by Andrei Halauneu on 29.10.2016.
 */
public class TestListener extends TestListenerAdapter {

    @Override
    public void onTestSuccess(ITestResult tr) {
        Browser.getBrowser().screenshot();
        Log.info(tr.getTestClass().getName() + "." + tr.getName() + " PASSED");
        Browser.getBrowser().close();
    }

    @Override
    public void onTestFailure(ITestResult tr) {
        Browser.getBrowser().screenshot();
        Log.error(tr.getTestClass().getName()
                + "." + tr.getName() + " failed", new Throwable(tr.getThrowable().getMessage()));
        Browser.getBrowser().close();
    }

    @Override
    public void onTestStart(ITestResult tr) {
        Log.info(tr.getTestClass().getName() + "." + tr.getName() + " STARTED");
    }

    public void onTestSkipped(ITestResult tr) {
        Log.warn(tr.getTestClass().getName() + "." + tr.getName() + " SKIPPED");
        Browser.getBrowser().close();
    }
}

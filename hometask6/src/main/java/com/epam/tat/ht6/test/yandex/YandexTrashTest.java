package com.epam.tat.ht6.test.yandex;

import com.epam.tat.ht6.framework.util.FileGenerator;
import com.epam.tat.ht6.product.yandex.account.bo.YandexAccountFactory;
import com.epam.tat.ht6.product.yandex.account.service.YandexLoginService;
import com.epam.tat.ht6.product.yandex.common.service.YandexTrashService;
import com.epam.tat.ht6.product.yandex.common.service.YandexUploadDownloadService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexTrashTest {
    private YandexLoginService yandexLoginService = new YandexLoginService();
    private YandexTrashService trashService = new YandexTrashService();
    private YandexUploadDownloadService upDownService = new YandexUploadDownloadService();
    private FileGenerator fileGenerator = new FileGenerator();
    private String fileName;
    private String filePath;
    private Map<String, String> fileList = new HashMap<>();

    @BeforeClass
    public void setUp() throws IOException {
        fileGenerator.generateFile();
        fileName = fileGenerator.getFileName();
        filePath = fileGenerator.getFilePath();
    }

    @BeforeMethod
    public void init() {
        yandexLoginService.login(YandexAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Moving files to trash")
    public void removeFileToTrashTest() throws IOException {
        upDownService.uploadFile(fileName, filePath);
        upDownService.checkFileUploaded(fileName);
        trashService.moveFileToTrash(fileName);
        trashService.checkFileInTrash(fileName);
    }

    @Test(description = "Deleting file from trash", dependsOnMethods = "removeFileToTrashTest")
    public void removeFilePermanentlyFromTrashTest() {
        trashService.deleteFileFromTrash(fileName);
        trashService.checkFileWasDeleted(fileName);
    }

    @Test(description = "Upload two files and check if success")
    public void uploadSeveralFiles() throws IOException {
        for (Map.Entry<String, String> entry : fileList.entrySet()) {
            upDownService.uploadFile(entry.getKey(), entry.getValue());
            upDownService.checkFileUploaded(entry.getKey());
        }
    }

    @Test(description = "Remove several files to trash section", dependsOnMethods = "uploadSeveralFiles")
    public void removeFilesToTrashTest() throws IOException {
        trashService.moveTwoFilesToTrash(fileList);
        trashService.checkTrashFolder(fileList);
    }
}

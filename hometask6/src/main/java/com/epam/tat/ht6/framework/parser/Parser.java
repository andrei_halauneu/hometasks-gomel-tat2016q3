package com.epam.tat.ht6.framework.parser;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.tat.ht6.framework.logging.Log;

/**
 * Created by Andrei Halauneu on 05.11.2016.
 */
public class Parser {

    public static void parseCli(String[] args) {
        JCommander jComm = new JCommander(Parameters.instance());
        try {
            jComm.parse(args);
        } catch (ParameterException e) {
            Log.error(e.getMessage(), e);
            System.exit(1);
        }
        if (Parameters.instance().isHelp()) {
            jComm.usage();
            System.exit(0);
        }
    }
}


package com.epam.tat.ht6.product.mailru.common.exception;


import com.epam.tat.ht6.framework.exception.CommonTestRuntimeException;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailSendException extends CommonTestRuntimeException {

    public MailSendException(String message) {
        super(message);
    }
}

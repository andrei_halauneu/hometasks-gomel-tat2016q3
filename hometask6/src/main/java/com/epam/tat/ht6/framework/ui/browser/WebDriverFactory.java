package com.epam.tat.ht6.framework.ui.browser;

import com.epam.tat.ht6.framework.parser.Parameters;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Andrei Halauneu on 04.11.2016.
 */
public class WebDriverFactory {
    private static final String REMOTE_URL = "http://127.0.0.1:4444/wd/hub";

    public static RemoteWebDriver getWebDriver() throws MalformedURLException {
        RemoteWebDriver remoteWebDriver;
        String url = "http://" + Parameters.instance().getHost() + ":"
                + Parameters.instance().getPort()
                + "/wd/hub";

        switch (Parameters.instance().getBrowserType()) {
            case FIREFOX:
                DesiredCapabilities dcFox = DesiredCapabilities.firefox();
                dcFox.setCapability(FirefoxDriver.PROFILE, BrowserProfiles.getFxProfile());
                remoteWebDriver = new RemoteWebDriver(new URL(url), dcFox);
                break;
            case CHROME:
                remoteWebDriver = new RemoteWebDriver(new URL(url), BrowserProfiles.getChromeProfile());
                System.setProperty("webdriver.chrome.driver", Parameters.instance().getChromeDriverPath());
                break;
            default:
                throw new RuntimeException("Browser unsupported:" + Parameters.instance().getBrowserType());
        }
        return remoteWebDriver;
    }
}

package com.epam.tat.ht6.product.yandex.account.exception;


import com.epam.tat.ht6.framework.exception.CommonTestRuntimeException;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexLoginException extends CommonTestRuntimeException {

    public YandexLoginException(String message) {
        super(message);
    }
}

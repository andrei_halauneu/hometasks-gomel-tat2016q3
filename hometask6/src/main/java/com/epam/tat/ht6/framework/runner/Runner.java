package com.epam.tat.ht6.framework.runner;

import com.epam.tat.ht6.framework.parser.Parameters;
import com.epam.tat.ht6.framework.parser.Parser;
import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrei Halauneu on 05.11.2016.
 */
public class Runner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        Parser.parseCli(args);
        List<String> files = Arrays.asList(Parameters.instance().getXmlSuitePath());
        testNG.setTestSuites(files);
        testNG.run();
    }
}

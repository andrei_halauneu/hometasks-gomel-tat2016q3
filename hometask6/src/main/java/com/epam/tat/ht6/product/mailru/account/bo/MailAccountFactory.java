package com.epam.tat.ht6.product.mailru.account.bo;


import com.epam.tat.ht6.framework.util.Randoms;
import com.epam.tat.ht6.product.mailru.common.MailGlobalParameters;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailAccountFactory {

    public static MailAccount createCorrectDataAccount() {
        MailAccount mailAccount = new MailAccount();
        mailAccount.setUsername(MailGlobalParameters.DEFAULT_ACCOUNT_USERNAME);
        mailAccount.setPassword(MailGlobalParameters.DEFAULT_ACCOUNT_PASSWORD);
        return mailAccount;
    }

    public static MailAccount createWrongDataAccount() {
        MailAccount mailAccount = new MailAccount();
        mailAccount.setPassword(Randoms.randomAlphabetic());
        mailAccount.setUsername(Randoms.randomAlphabetic());
        return mailAccount;
    }
}

package com.epam.tat.ht6.framework.runner;

import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrei Halauneu on 29.10.2016.
 */
public class MailRunner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> files = Arrays.asList("test-suites/mailru.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}

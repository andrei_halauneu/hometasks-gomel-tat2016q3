package com.epam.tat.ht6.product.mailru.common.service;


import com.epam.tat.ht6.product.mailru.common.screen.MailMainPage;
import com.epam.tat.ht6.product.mailru.common.screen.MailMenuPage;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class FindLetterService {
    private MailMainPage mailMainPage = new MailMainPage();
    private MailMenuPage mailMenuPage = new MailMenuPage();

    public boolean isMailFoundInInboxBySubject(String subject) {
        mailMenuPage.enterToInboxMail();
        return mailMainPage.findNeededMailBySubject(subject);
    }

    public boolean isMailFoundInSentBySubject(String subject) {
        mailMenuPage.enterToSentMail();
        return mailMainPage.findNeededMailBySubject(subject);
    }

    public boolean isMailFoundInDraftsBySubject(String subject) {
        mailMenuPage.enterToDraftsMail();
        return mailMainPage.findNeededMailBySubject(subject);
    }

    public boolean isMailFoundInTrashBySubject(String subject) {
        mailMenuPage.enterToTrashMail();
        return mailMainPage.findNeededMailBySubject(subject);
    }

    public boolean isMailFoundInInboxWithoutSubject() {
        mailMenuPage.enterToInboxMail();
        return mailMainPage.findNeededMailWithoutSubject();
    }

    public boolean isMailFoundInSentWithoutSubject() {
        mailMenuPage.enterToSentMail();
        return mailMainPage.findNeededMailWithoutSubject();
    }

}

package com.epam.tat.ht6.framework.logging;

import org.apache.log4j.Logger;

/**
 * Created by Andrei Halauneu on 05.11.2016.
 */
public class Log {
    private static final Logger LOG = Logger.getLogger("com.epam.tat.hw6");

    public static void info(Object message) {
        LOG.info(message);
    }

    public static void error(Object error, Throwable throwable) {
        LOG.error(error, throwable);
    }

    public static void debug(Object message) {
        LOG.debug(message);
    }

    public static void warn(Object message) {
        LOG.warn(message);
    }
}


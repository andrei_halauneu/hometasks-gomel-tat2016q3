package com.epam.tat.ht6.framework.ui.browser;

/**
 * Created by Andrei Halauneu on 03.11.2016.
 */
public enum BrowserType {
    CHROME,
    FIREFOX
}

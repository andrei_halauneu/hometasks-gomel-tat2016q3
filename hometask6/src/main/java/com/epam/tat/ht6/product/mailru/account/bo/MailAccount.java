package com.epam.tat.ht6.product.mailru.account.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailAccount {
    @Getter
    @Setter
    private String username;
    @Getter
    @Setter
    private String password;

}

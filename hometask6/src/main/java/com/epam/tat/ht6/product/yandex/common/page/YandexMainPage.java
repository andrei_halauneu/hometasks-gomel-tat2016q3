package com.epam.tat.ht6.product.yandex.common.page;

import com.epam.tat.ht6.framework.ui.element.Element;
import com.epam.tat.ht6.framework.ui.element.ElementAction;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;

import java.util.Map;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexMainPage {
    private static final String FILE_NAME_LOCATOR = "//div[contains(@title,'%s')]";
    private static final By TRASH_LOCATOR = By.xpath("//div[@title='Корзина']");
    private static final By IN_TRASH_SECTION_CHECK_LOCATOR = By.xpath("//span[@class='crumbs__current']");
    private Element file;
    private Element trash = new Element(TRASH_LOCATOR);
    private Element checkInTrashSection = new Element(IN_TRASH_SECTION_CHECK_LOCATOR);

    public YandexMainPage findFile(String fileName) throws NoSuchElementException {
        file = new Element(By.xpath(String.format(FILE_NAME_LOCATOR, fileName)));
        file.waitForAppear();
        return this;
    }

    public YandexMainPage waitForFileNotInSection(String fileName) throws NoSuchElementException {
        file = new Element(By.xpath(String.format(FILE_NAME_LOCATOR, fileName)));
        file.waitForDisappear();
        return this;
    }

    public YandexMainPage pickFile() {
        file.waitForAppear();
        file.click();
        return this;
    }

    public YandexMainPage dragFileToTrash() {
        ElementAction action = new ElementAction();
        trash.waitForAppear();
        action.clickAndHold(file)
                .moveToElement(trash)
                .release()
                .buildAndPerform();
        return this;
    }

    public YandexMainPage dragTwoFileToTrash(Map<String, String> filesList) {
        ElementAction action = new ElementAction();
        trash.waitForAppear();
        for (Map.Entry<String, String> entry : filesList.entrySet()) {
            file = new Element(By.xpath(String.format(FILE_NAME_LOCATOR, entry.getKey())));
            file.waitForAppear();
            action
                    .keyDown(Keys.CONTROL)
                    .click(file)
                    .buildAndPerform();
        }
        action
                .clickAndHold(file)
                .moveToElement(trash)
                .release()
                .buildAndPerform();
        return this;
    }

    public YandexMainPage findSeveralFiles(Map<String, String> filesList) throws NoSuchElementException {
        for (Map.Entry<String, String> entry : filesList.entrySet()) {
            file = new Element(By.xpath(String.format(FILE_NAME_LOCATOR, entry.getKey())));
            file.waitForAppear();
        }
        return this;
    }

    public YandexMainPage enterToTrash() {
        ElementAction action = new ElementAction();
        trash.waitForAppear();
        action.doubleClick(trash).buildAndPerform();
        checkInTrashSection.waitForAppear();
        return this;
    }

}

package com.epam.tat.ht6.product.yandex.common;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexGlobalParameters {

    public static final String DEFAULT_PRODUCT_URL = "https://disk.yandex.ru/";
    public static final String DEFAULT_ACCOUNT_USERNAME = "tat-hwrk";
    public static final String DEFAULT_ACCOUNT_PASSWORD = "qwe123asd";
    public static final String FILE_PATH = "./src/main/resources/attach.txt";
    public static final String FILE_NAME = "attach";
    public static final String EXTENSION = ".txt";
    public static final int DOWNLOAD_TIMEOUT = 3000;

}


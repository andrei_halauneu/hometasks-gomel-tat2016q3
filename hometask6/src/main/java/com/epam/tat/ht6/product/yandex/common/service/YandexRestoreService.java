package com.epam.tat.ht6.product.yandex.common.service;


import com.epam.tat.ht6.product.yandex.common.exception.YandexFailedOperationException;
import com.epam.tat.ht6.product.yandex.common.page.YandexMainPage;
import com.epam.tat.ht6.product.yandex.common.page.YandexLeftFramePage;
import com.epam.tat.ht6.product.yandex.common.page.YandexRightFramePage;
import org.openqa.selenium.NoSuchElementException;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexRestoreService {
    private YandexMainPage yandexMainPage = new YandexMainPage();
    private YandexRightFramePage yandexRightFramePage = new YandexRightFramePage();
    private YandexLeftFramePage yandexLeftFramePage = new YandexLeftFramePage();

    public void restoreFile(String fileName) {
        yandexMainPage
                .enterToTrash()
                .findFile(fileName)
                .pickFile();
        yandexRightFramePage.restoreFile();
    }

    public void checkFileRestoreToFilesSection(String fileName) {
        yandexLeftFramePage.enterToFilesSection();
        try {
            yandexMainPage.findFile(fileName);
        } catch (NoSuchElementException e) {
            throw new YandexFailedOperationException("File was not remove to trash section");
        }
    }
}

package com.epam.tat.ht6.framework.parser;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.epam.tat.ht6.framework.ui.browser.BrowserType;
import lombok.Getter;

/**
 * Created by Andrei Halauneu on 05.11.2016.
 */
public class Parameters {
    private static Parameters instance;

    @Parameter(description = "How to use", names = "--help", help = true)
    @Getter
    private boolean help;

    @Parameter(description = "Define browser, for chrome enter(-cp) path to chromedriver.exe",
            names = {"--browser", "-b"},
            converter = BrowserTypeConverter.class)
    @Getter
    private BrowserType browserType = BrowserType.FIREFOX;

    @Parameter(description = "Path to chrome driver", names = {"--chromepath", "-cp"})
    @Getter
    private String chromeDriverPath = "./src/main/resources/driver/chromedriver.exe";

    @Parameter(description = "Selenium host for remote webdriver:", names = {"--host", "-h"}, required = true)
    @Getter
    private String host;

    @Parameter(description = "Selenium port for remote webdriver", names = {"--port", "-p"}, required = true)
    @Getter
    private String port;

    @Parameter(description = "Path to xml suite file", names = {"--suite", "-st"}, required = true)
    @Getter
    private String xmlSuitePath;

    public static Parameters instance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public static class BrowserTypeConverter implements IStringConverter<BrowserType> {
        @Override
        public BrowserType convert(String s) {
            return BrowserType.valueOf(s.toUpperCase());
        }
    }
}


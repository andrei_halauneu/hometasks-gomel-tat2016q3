package com.epam.tat.ht6.framework.ui.browser;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.util.HashMap;

/**
 * Created by Andrei Halauneu on 29.10.2016.
 */
public class BrowserProfiles {
    private static final String DOWNLOADS_PATH = "./src/main/resources/downloads";

    public static FirefoxProfile getFxProfile() {
        FirefoxProfile fxProfile = new FirefoxProfile();
        fxProfile.setPreference("browser.download.folderList", 2);
        fxProfile.setPreference("browser.download.manager.showWhenStarting", false);
        fxProfile.setPreference("browser.download.dir", new File(DOWNLOADS_PATH).getAbsolutePath());
        fxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain,image/jpeg");
        return fxProfile;
    }

    public static DesiredCapabilities getChromeProfile() {
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", new File(DOWNLOADS_PATH).getAbsolutePath());
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        return capabilities;
    }
}

package com.epam.tat.ht6.product.mailru.common;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailGlobalParameters {
    public static final String DEFAULT_PRODUCT_URL = "https://mail.ru/";
    public static final String DEFAULT_ACCOUNT_USERNAME = "tat-hwrk";
    public static final String DEFAULT_ACCOUNT_EMAIL = "tat-hwrk@mail.ru";
    public static final String DEFAULT_ACCOUNT_PASSWORD = "qwe123asd";
    public static final String LOCATION_FILE = "./src/main/resources/attach.txt";
}

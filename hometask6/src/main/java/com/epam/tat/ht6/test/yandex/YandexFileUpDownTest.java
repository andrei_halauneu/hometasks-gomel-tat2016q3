package com.epam.tat.ht6.test.yandex;

import com.epam.tat.ht6.framework.util.FileGenerator;
import com.epam.tat.ht6.product.yandex.account.bo.YandexAccountFactory;
import com.epam.tat.ht6.product.yandex.account.service.YandexLoginService;
import com.epam.tat.ht6.product.yandex.common.service.YandexUploadDownloadService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexFileUpDownTest {
    private YandexLoginService yandexLoginService = new YandexLoginService();
    private YandexUploadDownloadService upDownService = new YandexUploadDownloadService();
    private FileGenerator generator = new FileGenerator();
    private String fileName;
    private String filePath;

    @BeforeClass
    public void setUp() throws IOException {
        generator.generateFile();
        fileName = generator.getFileName();
        filePath = generator.getFilePath();
    }

    @BeforeMethod
    public void init() {
        yandexLoginService.login(YandexAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Uploading file and checking for success")
    public void uploadTest() {
        upDownService.uploadFile(fileName, filePath);
        upDownService.checkFileUploaded(fileName);
    }

    @Test(description = "Downloading file", dependsOnMethods = "uploadTest")
    public void downloadTest() throws IOException {
        upDownService.downloadFile(fileName);
        upDownService.checkFileContent(fileName);
        upDownService.deleteDownloadDirectory();
    }
}

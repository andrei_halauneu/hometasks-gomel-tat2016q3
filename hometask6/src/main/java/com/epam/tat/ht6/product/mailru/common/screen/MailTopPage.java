package com.epam.tat.ht6.product.mailru.common.screen;


import com.epam.tat.ht6.framework.ui.element.Element;
import com.epam.tat.ht6.framework.ui.element.ElementAction;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailTopPage {
    private static final By WRITE_MAIL_BUTTON_LOCATOR
            = By.xpath("//a[@class='b-toolbar__btn js-shortcut']");
    private static final By SEND_MAIL_BUTTON_LOCATOR = By.xpath("//div[@data-name='send']");
    private static final By SAVE_DRAFT_BUTTON_LOCATOR = By.xpath("//div[@data-name='saveDraft']");
    private Element writeMailButton = new Element(WRITE_MAIL_BUTTON_LOCATOR);
    private Element sendMailButton = new Element(SEND_MAIL_BUTTON_LOCATOR);
    private Element saveToDraftButton = new Element(SAVE_DRAFT_BUTTON_LOCATOR);

    public void openToWriteMail() {
        writeMailButton.waitForAppear();
        writeMailButton.click();
    }

    public void sendMail() {
        sendMailButton.waitForAppear();
        sendMailButton.click();
    }

    public void saveInDraftMail() {
        saveToDraftButton.click();
    }

    public void deleteSelectedMail() {
        ElementAction elementAction = new ElementAction();
        elementAction.actOnElement()
                .sendKeys(Keys.DELETE)
                .build()
                .perform();
    }

}



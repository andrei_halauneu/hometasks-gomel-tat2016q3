package com.epam.tat.ht6.framework.util;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by Andrei Halauneu on 29.10.2016.
 */
public class Randoms {
    public static final int DEFAULT_LENGTH = 10;

    public static String randomAlphabetic() {
        return RandomStringUtils.randomAlphabetic(DEFAULT_LENGTH);
    }

}

package com.epam.tat.ht6.product.mailru.account.service;


import com.epam.tat.ht6.product.mailru.account.bo.MailAccount;
import com.epam.tat.ht6.product.mailru.account.exception.MailLoginException;
import com.epam.tat.ht6.product.mailru.account.screen.MailLoginPage;
import com.epam.tat.ht6.product.mailru.common.screen.MailUserbar;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailLoginService {

    public void login(MailAccount mailAccount) {
        unsafeLogin(mailAccount);
        checkLoginSuccess();
    }

    public void unsafeLogin(MailAccount mailAccount) {
        MailLoginPage mailLoginPage = new MailLoginPage();
        mailLoginPage.open()
                .setLogin(mailAccount.getUsername())
                .setPassword(mailAccount.getPassword())
                .logIn();
    }

    public void checkLoginSuccess() {
        if (!isLoggedSuccess()) {
            throw new MailLoginException("Login failed: " + new MailLoginPage().getErrorMessage());
        }
    }

    public boolean isLoggedSuccess() {
        return new MailUserbar().isLoggedIn();
    }

    public void checkErrorMessage(String expectedMessage) {
        String actualMessage = new MailLoginPage().getErrorMessage();
        if (!actualMessage.equals(expectedMessage)) {
            throw new MailLoginException(String.format(
                    "Expected message is '%s' but actual is '%s'", expectedMessage, actualMessage));
        }
    }

}

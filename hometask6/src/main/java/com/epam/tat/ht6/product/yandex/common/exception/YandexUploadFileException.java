package com.epam.tat.ht6.product.yandex.common.exception;

import com.epam.tat.ht6.framework.exception.CommonTestRuntimeException;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexUploadFileException extends CommonTestRuntimeException {

    public YandexUploadFileException(String message) {
        super(message);
    }
}

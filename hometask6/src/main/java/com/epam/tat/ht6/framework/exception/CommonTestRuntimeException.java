package com.epam.tat.ht6.framework.exception;

/**
 * Created by Andrei Halauneu on 29.10.2016.
 */
public class CommonTestRuntimeException extends RuntimeException {

    public CommonTestRuntimeException(String message) {
        super(message);
    }

    public CommonTestRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}

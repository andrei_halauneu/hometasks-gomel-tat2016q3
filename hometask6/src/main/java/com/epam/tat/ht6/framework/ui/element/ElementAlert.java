package com.epam.tat.ht6.framework.ui.element;

import com.epam.tat.ht6.framework.ui.browser.Browser;
import org.openqa.selenium.Alert;

/**
 * Created by Andrei Halauneu on 29.10.2016.
 */
public class ElementAlert {

    public void acceptAlert() {
        Alert alert = Browser.getBrowser().getWrappedDriver().switchTo().alert();
        alert.accept();
    }

    public String getAlertText() {
        Alert alert = Browser.getBrowser().getWrappedDriver().switchTo().alert();
        return alert.getText();
    }
}

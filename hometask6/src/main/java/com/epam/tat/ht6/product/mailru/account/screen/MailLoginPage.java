package com.epam.tat.ht6.product.mailru.account.screen;

import com.epam.tat.ht6.framework.ui.browser.Browser;
import com.epam.tat.ht6.framework.ui.element.Element;
import com.epam.tat.ht6.product.mailru.common.MailGlobalParameters;
import org.openqa.selenium.By;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailLoginPage {
    private static final String URL = MailGlobalParameters.DEFAULT_PRODUCT_URL;
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@id='mailbox__login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@id='mailbox__password']");
    private static final By BUTTON_LOGIN_LOCATOR = By.xpath("//input[@id='mailbox__auth__button']");
    private static final By LOGIN_ERROR_MESSAGE_LOCATOR = By.xpath("//div[@class='b-login__errors']");

    private Element loginInput = new Element(LOGIN_INPUT_LOCATOR);
    private Element passwordInput = new Element(PASSWORD_INPUT_LOCATOR);
    private Element buttonLogin = new Element(BUTTON_LOGIN_LOCATOR);
    private Element errorMessage = new Element(LOGIN_ERROR_MESSAGE_LOCATOR);

    public MailLoginPage open() {
        Browser.getBrowser().open(URL);
        return this;
    }

    public MailLoginPage setLogin(String login) {
        loginInput.typeValue(login);
        return this;
    }

    public MailLoginPage setPassword(String password) {
        passwordInput.typeValue(password);
        return this;
    }

    public void logIn() {
        buttonLogin.click();
    }

    public String getErrorMessage() {
        return errorMessage.getText();
    }

}

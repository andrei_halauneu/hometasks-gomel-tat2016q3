package com.epam.tat.ht6.product.mailru.common.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailLetter {
    @Getter
    @Setter
    private String address;
    @Getter
    @Setter
    private String subject;
    @Getter
    @Setter
    private String text;


}

package com.epam.tat.ht6.framework.ui.browser;

import com.epam.tat.ht6.framework.exception.CommonTestRuntimeException;
import com.epam.tat.ht6.framework.logging.Log;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Created by Andrei Halauneu on 29.10.2016.
 */
public class Browser implements WrapsDriver{
    public static final int ELEMENT_WAIT_TIMEOUT_SECONDS = 100;

    private static ThreadLocal<Browser> instance = new ThreadLocal();
    private RemoteWebDriver remoteDriver;

    private Browser() {
        try {
            remoteDriver = WebDriverFactory.getWebDriver();
        } catch (MalformedURLException e) {
            throw new RuntimeException("Wrong local host");
        }
        remoteDriver.manage().window().maximize();
    }

    public static synchronized Browser getBrowser() {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    public void close() {
        try {
            if (getWrappedDriver() != null) {
                getWrappedDriver().quit();
            }
        } finally {
            instance = null;
        }
    }

    @Override
    public WebDriver getWrappedDriver() {
        return remoteDriver;
    }

    public void open(String url) {
        remoteDriver.get(url);
    }


    public WebElement findElement(By by) {
        return remoteDriver.findElement(by);
    }

    public boolean isElementPresent(By by) {
        return remoteDriver.findElements(by).size() > 0;
    }

    public boolean isElementVisible(By by) {
        if (!isElementPresent(by)) {
            return false;
        }
        return findElement(by).isDisplayed();
    }

    public Actions getActions() {
        return new Actions(Browser.getBrowser().getWrappedDriver());
    }

    public void waitForElementIsPresent(By by) {
        WebDriverWait wait = new WebDriverWait(remoteDriver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public void waitForElementIsInvisible(By by) {
        WebDriverWait wait = new WebDriverWait(remoteDriver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public void waitForElementIsVisible(By by) {
        WebDriverWait wait = new WebDriverWait(remoteDriver, ELEMENT_WAIT_TIMEOUT_SECONDS);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void switchToFrame(WebElement frame) {
        remoteDriver.switchTo().frame(frame);
    }

    public void switchToMainFrame() {
        remoteDriver.switchTo().defaultContent();
    }

    public byte[] screenshot() {
        if (instance == null) {
            throw new CommonTestRuntimeException("Screenshot failed, no browser");
        }
        File screenshotFile = new File("logs/screenshots/" + System.nanoTime() + ".png");
        try {
            byte[] screenshotBytes = remoteDriver.getScreenshotAs(OutputType.BYTES);
            FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
            Log.info("Screenshot taken: file://" + screenshotFile.getAbsolutePath());
            return screenshotBytes;
        } catch (IOException e) {
            throw new CommonTestRuntimeException("Screenshot take failed", e);
        }
    }
}

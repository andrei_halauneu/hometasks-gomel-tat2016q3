package com.epam.tat.ht6.product.yandex.account.service;


import com.epam.tat.ht6.product.yandex.account.bo.YandexAccount;
import com.epam.tat.ht6.product.yandex.account.exception.YandexLoginException;
import com.epam.tat.ht6.product.yandex.account.page.YandexLoginPage;
import com.epam.tat.ht6.product.yandex.common.page.YandexHeaderPage;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexLoginService {

    public void login(YandexAccount yandexAccount) {
        unsafeLogin(yandexAccount);
        checkLoginSuccess();
    }

    public void unsafeLogin(YandexAccount yandexAccount) {
        YandexLoginPage yandexLoginPage = new YandexLoginPage();
        yandexLoginPage.open()
                .setLogin(yandexAccount.getUsername())
                .setPassword(yandexAccount.getPassword())
                .signIn();
    }

    public void checkLoginSuccess() {
        if (!isLoggedSuccess()) {
            throw new YandexLoginException("Authentication failed: ");
        }
    }

    public boolean isLoggedSuccess() {
        return new YandexHeaderPage().isLoggedIn();
    }

    public boolean checkFailedLogin(boolean expectedResult) {
        boolean actualResult = new YandexLoginPage().checkFailedLogin();
        if (!actualResult == expectedResult) {
            throw new YandexLoginException(String.format(
                    "Expected result is '%s' but actual is '%s'", expectedResult, actualResult));
        }
        return actualResult;
    }

}

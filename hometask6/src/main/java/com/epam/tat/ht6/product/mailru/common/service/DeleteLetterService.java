package com.epam.tat.ht6.product.mailru.common.service;


import com.epam.tat.ht6.product.mailru.common.exception.MailDeleteException;
import com.epam.tat.ht6.product.mailru.common.screen.MailMainPage;
import com.epam.tat.ht6.product.mailru.common.screen.MailMenuPage;
import com.epam.tat.ht6.product.mailru.common.screen.MailTopPage;
import org.openqa.selenium.TimeoutException;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class DeleteLetterService {
    private MailMainPage mailMainPage = new MailMainPage();
    private MailMenuPage mailMenuPage = new MailMenuPage();
    private MailTopPage mailTopPage = new MailTopPage();

    public void deleteNeededMailFromDrafts(String subject) {
        mailMenuPage.enterToDraftsMail();
        mailMainPage.clickNeededMailCheckbox(subject);
        mailTopPage.deleteSelectedMail();
    }

    public void deleteAllMailInTrash() {
        mailMenuPage.cleanAllMailInTrash();
    }

    public void checkMailDeletedFromTrash(String subject) {
        mailMenuPage.enterToTrashMail();
        try {
            mailMainPage.waitDisappearNeededMailBySubject(subject);
        } catch (TimeoutException e) {
            throw new MailDeleteException("MailLetter delete failed");
        }
    }

    public void checkMailDeletedFromDrafts(String subject) {
        mailMenuPage.enterToDraftsMail();
        try {
            mailMainPage.waitDisappearNeededMailBySubject(subject);
        } catch (TimeoutException e) {
            throw new MailDeleteException("MailLetter delete failed");
        }
    }
}

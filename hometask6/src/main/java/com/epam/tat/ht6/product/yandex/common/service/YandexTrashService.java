package com.epam.tat.ht6.product.yandex.common.service;

import com.epam.tat.ht6.product.yandex.common.exception.YandexFailedOperationException;
import com.epam.tat.ht6.product.yandex.common.page.YandexMainPage;
import com.epam.tat.ht6.product.yandex.common.page.YandexRightFramePage;
import org.openqa.selenium.NoSuchElementException;

import java.util.Map;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexTrashService {
    private YandexMainPage yandexMainPage = new YandexMainPage();
    private YandexRightFramePage yandexRightFramePage = new YandexRightFramePage();

    public void moveFileToTrash(String fileName) {
        yandexMainPage
                .findFile(fileName)
                .pickFile()
                .dragFileToTrash();
    }

    public void moveTwoFilesToTrash(Map<String, String> filesList) {
        yandexMainPage.dragTwoFileToTrash(filesList);
    }

    public void checkFileInTrash(String fileName) {
        yandexMainPage.enterToTrash();
        try {
            yandexMainPage.findFile(fileName);
        } catch (NoSuchElementException e) {
            throw new YandexFailedOperationException("File wasn't removed to trash");
        }
    }

    public void checkTrashFolder(Map<String, String> filesList) {
        yandexMainPage.enterToTrash();
        try {
            yandexMainPage.findSeveralFiles(filesList);
        } catch (NoSuchElementException e) {
            throw new YandexFailedOperationException("File wasn't removed to trash");
        }
    }

    public void deleteFileFromTrash(String file) {
        yandexMainPage
                .enterToTrash()
                .findFile(file)
                .pickFile();
        yandexRightFramePage.deleteFile();
    }

    public void checkFileWasDeleted(String fileName) {
        yandexMainPage.enterToTrash();
        try {
            yandexMainPage.waitForFileNotInSection(fileName);
        } catch (NoSuchElementException e) {
            throw new YandexFailedOperationException("File wasn't deleted");
        }
    }
}

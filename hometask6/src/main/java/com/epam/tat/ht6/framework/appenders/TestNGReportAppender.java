package com.epam.tat.ht6.framework.appenders;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.testng.Reporter;

/**
 * Created by Andrei Halauneu on 03.11.2016.
 */
public class TestNGReportAppender extends AppenderSkeleton {

    @Override
    public boolean requiresLayout() {
        return true;
    }

    @Override
    protected void append(LoggingEvent loggingEvent) {
        Reporter.log(layout.format(loggingEvent));
    }

    @Override
    public void close() {
    }
}

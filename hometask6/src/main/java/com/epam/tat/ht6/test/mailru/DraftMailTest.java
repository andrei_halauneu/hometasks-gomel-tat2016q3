package com.epam.tat.ht6.test.mailru;


import com.epam.tat.ht6.product.mailru.account.bo.MailAccountFactory;
import com.epam.tat.ht6.product.mailru.account.service.MailLoginService;
import com.epam.tat.ht6.product.mailru.common.bo.MailLetter;
import com.epam.tat.ht6.product.mailru.common.bo.MailLetterFactory;
import com.epam.tat.ht6.product.mailru.common.service.DeleteLetterService;
import com.epam.tat.ht6.product.mailru.common.service.FindLetterService;
import com.epam.tat.ht6.product.mailru.common.service.SaveLetterService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class DraftMailTest {
    private MailLoginService mailLoginService = new MailLoginService();
    private SaveLetterService saveLetterService = new SaveLetterService();
    private FindLetterService findLetterService = new FindLetterService();
    private DeleteLetterService deleteMailInDrafts = new DeleteLetterService();
    private String subjectForMail;

    @BeforeMethod
    public void init() {
        mailLoginService.login(MailAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Saving letter to drafts")
    public void saveMailInDraftsTest() {
        MailLetter mailLetter = MailLetterFactory.createMailWithAllFields();
        Boolean actualResult = saveLetterService.isMailSavedInDrafts(mailLetter);
        subjectForMail = mailLetter.getSubject();
        Assert.assertTrue(actualResult, "saveMailInDraftsTest error");
    }

    @Test(description = "Checking letter with subject in drafts",
            dependsOnMethods = "saveMailInDraftsTest")
    public void isMailInDraftsTest() {
        Boolean actualResult = findLetterService.isMailFoundInDraftsBySubject(subjectForMail);
        Assert.assertTrue(actualResult, "isMailWithInDraftsTest error");
    }

    @Test(description = "Deleting mail with subject from drafts",
            dependsOnMethods = "isMailInDraftsTest")
    public void deleteMailInDraftWithSubjectTest() {
        deleteMailInDrafts.deleteNeededMailFromDrafts(subjectForMail);
        deleteMailInDrafts.checkMailDeletedFromDrafts(subjectForMail);
    }

    @Test(description = "Checking mail in trash folder",
            dependsOnMethods = "deleteMailInDraftWithSubjectTest")
    public void isMailInTrashSectionTest() {
        Boolean actualResult = findLetterService.isMailFoundInTrashBySubject(subjectForMail);
        Assert.assertTrue(actualResult, "isMailInTrashSectionTest error");
    }

    @Test(description = "Clearing trash folder",
            dependsOnMethods = "isMailInTrashSectionTest")
    public void clearTrashFolder() {
        deleteMailInDrafts.deleteAllMailInTrash();
        deleteMailInDrafts.checkMailDeletedFromTrash(subjectForMail);
    }
}

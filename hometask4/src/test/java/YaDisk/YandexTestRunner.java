package YaDisk;

import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrei Halauneu on 04.10.2016.
 */
public class YandexTestRunner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> files = Arrays.asList(
                "./src/test/resources/suites/YandexTest.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}

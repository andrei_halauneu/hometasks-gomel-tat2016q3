package YaDisk;

import com.epam.tat.ht4.pages.ya.*;
import com.epam.tat.ht4.utils.BrowserProfiles;
import com.epam.tat.ht4.utils.FileDownloadValidator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.epam.tat.ht4.utils.DriverTimeouts.IMPLICIT_WAIT;
import static com.epam.tat.ht4.utils.DriverTimeouts.PAGE_LOAD;

/**
 * Created by Andrei Halauneu on 04.10.2016.
 */
public class YandexTest {
    private static final String SITE = "https://disk.yandex.ru/";
    private static final String YA_LOGIN = "tat-hwrk";
    private static final String YA_PASSWORD = "qwe123asd";
    private static final String FILE_NAME = "attachment";
    private static final String EXTENSION = ".txt";
    private static final int DOWNLOAD_TIMEOUT = 3000;
    private WebDriver browser;
    MainPage mainPage = new MainPage(browser);

    @BeforeClass
    private void setUpDriver() {
        BrowserProfiles browserProperties = new BrowserProfiles();
        //webDriver = new FirefoxDriver(browserProperties.getFirefoxProperties());
        browser = new ChromeDriver(browserProperties.getChromeProfile());
        browser.manage().timeouts().implicitlyWait(IMPLICIT_WAIT.getValue(), IMPLICIT_WAIT.getUnit());
        browser.manage().timeouts().pageLoadTimeout(PAGE_LOAD.getValue(), PAGE_LOAD.getUnit());
        browser.manage().window().maximize();
    }

    @BeforeClass(dependsOnMethods = "setUpDriver")
    public void loginToYandex() {
        browser.get(SITE);
        LoginPage loginPage = new LoginPage(browser);
        loginPage.login(YA_LOGIN, YA_PASSWORD);
    }

    @Test(description = "Uploading file to Yandex Disk")
    private void uploadTest() {
        FileUploadedPopUp uploadedPopUp = mainPage.uploadFile(FILE_NAME, EXTENSION);
        Assert.assertTrue(uploadedPopUp.isFileUploadedSuccess(FILE_NAME), "Failed to upload file");
    }

    @Test(description = "Downloading file from Yandex Disk and validate download",
            dependsOnMethods = "uploadTest")
    private void downloadFileTest() throws InterruptedException {
        mainPage.downloadFile(FILE_NAME);
        Thread.sleep(DOWNLOAD_TIMEOUT);
        FileDownloadValidator downloadFileChecker = new FileDownloadValidator();
        Assert.assertTrue(downloadFileChecker.isFileDownloaded(FILE_NAME + EXTENSION, "Failed to download file" ));
    }

    @Test(description = "Moving file to trash folder", dependsOnMethods = "uploadTest")
    private void clickAndRemoveFileTest() {
        FileMovedToTrashPopUp movedToTrashPopUp= mainPage.moveFileToTrash(FILE_NAME);
        String fileRemovedToTrashResult = String.format("Файл «%s%s» удален в Корзину", FILE_NAME, EXTENSION);
        Assert.assertEquals(movedToTrashPopUp.getFileToTrashMessage(FILE_NAME, EXTENSION),
                fileRemovedToTrashResult, "Failed to remove file to trash");
    }

    @Test(description = "Restoring file from trash", dependsOnMethods = "clickAndRemoveTest")
    private void restoreFileTest() {
        FileRestoredPopUp fileRestoredPopUp = mainPage.restoreFile(FILE_NAME);
        String fileRestoreResult = String.format("Файл «%s%s» восстановлен", FILE_NAME, EXTENSION);
        Assert.assertEquals(fileRestoredPopUp.getFileRestoreSuccessMessage(FILE_NAME, EXTENSION),
                fileRestoreResult, "Failed to restore file from trash");
    }

    @Test(description = "Permanent file deletion from trash folder", dependsOnMethods = "clickAndRemoveTest")
    private void deletingFileTest() {
        mainPage.openFilesFolder();
        mainPage.moveFileToTrash(FILE_NAME);
        FileDeletedPopUp fileDeletedPopUp = mainPage.deleteFile(FILE_NAME);
        String fileDeletedResult = String.format("Файл «%s%s» был удален", FILE_NAME, EXTENSION);
        Assert.assertEquals(fileDeletedPopUp.getFileDeletedPermanentlySuccessMessage(FILE_NAME, EXTENSION),
                fileDeletedResult, "Failed fail deletion");
    }

    @Test(description = "Moving two files to trash")
    private void moveTwoFiles() {
        String first = "Мишки";
        String second = "Море";
        MultiDeletedPopUp multiDeletedPopUp = mainPage.deleteTwoFiles(first, second);
        Assert.assertEquals(multiDeletedPopUp.getFileDeletedPermanentlySuccessMessage(),
                "2 файла удалены в Корзину", "Failed to remove several files to trash");
    }

    @AfterClass
    private void driverQuit() {
        browser.quit();
    }
}

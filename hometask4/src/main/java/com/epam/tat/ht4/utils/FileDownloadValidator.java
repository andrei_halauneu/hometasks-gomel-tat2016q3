package com.epam.tat.ht4.utils;

import java.io.File;

/**
 * Created by Andrei Halauneu on 04.10.2016.
 * solution from
 * https://www.seleniumeasy.com/
 * selenium-tutorials/
 * verify-file-after-downloading-using-webdriver-java
 */
public class FileDownloadValidator {
    public boolean isFileDownloaded(String downloadPath, String fileName) {
        boolean flag = false;
        File dir = new File(downloadPath);
        File[] dirContents = dir.listFiles();

        for (int i = 0; i < dirContents.length; i++) {
            if (dirContents[i].getName().equals(fileName)) {
                return flag = true;
            }
            return flag;
        }
        return flag;
    }
}


package com.epam.tat.ht4.pages.ya;

import com.epam.tat.ht4.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Andrei Halauneu on 04.10.2016.
 */
public class LoginPage extends AbstractPage {
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='password']");
    private static final By BUTTON_LOGIN_LOCATOR = By.xpath("//div[@class='nb-input-group']//button");

    public LoginPage(WebDriver browser) {
        super(browser);
    }

    public void login(String yaLogin, String yaPassword) {
        browser.findElement(LOGIN_INPUT_LOCATOR).sendKeys(yaLogin);
        browser.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(yaPassword);
        browser.findElement(BUTTON_LOGIN_LOCATOR).click();
    }
}


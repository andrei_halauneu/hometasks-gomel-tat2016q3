package com.epam.tat.ht4.pages.ya;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Andrei Halauneu on 04.10.2016.
 */
public class FileDeletedPopUp extends MainPage {
    private static final String FILE_DELETED_LOCATOR = "//div[contains(text(),'Файл «%s%s» был удален')]";

    public FileDeletedPopUp(WebDriver browser) {
        super(browser);
    }

    public String getFileDeletedPermanentlySuccessMessage(String filename, String extension) {
        PageFactory.initElements(browser, FileDeletedPopUp.class);
        waitForElementLoad(By.xpath(String.format(FILE_DELETED_LOCATOR, filename, extension)));
        return browser.findElement(By.xpath(String.
                format(FILE_DELETED_LOCATOR, filename, extension))).getText();
    }
}


package com.epam.tat.ht4.pages.ya;

import com.epam.tat.ht4.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Andrei Halauneu on 04.10.2016.
 */
public class FileUploadedPopUp extends AbstractPage {
    private static final String FILE_NAME_LOCATOR = "//div[contains(text(),'%s')]";
    private static final By SUCCESS_UPLOAD_FILE_MESSAGE = By.xpath("//*[contains(text(),'Все загрузки завершены')]");

    public FileUploadedPopUp(WebDriver browser) {
        super(browser);
    }

    public boolean isFileUploadedSuccess(String filename) {
        PageFactory.initElements(browser, FileUploadedPopUp.class);
        waitForElementVisible(SUCCESS_UPLOAD_FILE_MESSAGE);
        waitForElementVisible(By.xpath(String.format(FILE_NAME_LOCATOR, filename)));
        return browser.findElement(By.xpath(String.format(FILE_NAME_LOCATOR, filename))).isDisplayed();
    }
}

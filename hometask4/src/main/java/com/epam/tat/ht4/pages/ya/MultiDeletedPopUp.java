package com.epam.tat.ht4.pages.ya;

import com.epam.tat.ht4.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Andrei Halauneu on 04.10.2016.
 */
public class MultiDeletedPopUp extends AbstractPage {
    private static final By SEVERAL_FILES_DELETED_LOCATOR = By.xpath("//div[contains(text(),'файла удалены')]");

    public MultiDeletedPopUp(WebDriver browser) {
        super(browser);
    }

    public String getFileDeletedPermanentlySuccessMessage() {
        PageFactory.initElements(browser, MultiDeletedPopUp.class);
        waitForElementVisible(SEVERAL_FILES_DELETED_LOCATOR);
        return browser.findElement(SEVERAL_FILES_DELETED_LOCATOR).getText();
    }
}

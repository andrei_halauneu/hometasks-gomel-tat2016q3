package com.epam.tat.ht4.utils;

import java.util.concurrent.TimeUnit;

/**
 * Created by Andrei Halauneu on 30.09.2016.
 */
public enum DriverTimeouts {

    IMPLICIT_WAIT(1, TimeUnit.SECONDS),
    PAGE_LOAD(35, TimeUnit.SECONDS),
    EXPLICIT_SECONDS(25, TimeUnit.SECONDS);

    private int value;
    private TimeUnit unit;

    DriverTimeouts(int value, TimeUnit unit) {
        this.value = value;
        this.unit = unit;
    }

    public int getValue() {
        return value;
    }

    public TimeUnit getUnit() {
        return unit;
    }
}

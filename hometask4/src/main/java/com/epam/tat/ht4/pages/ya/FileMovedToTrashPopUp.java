package com.epam.tat.ht4.pages.ya;

import com.epam.tat.ht4.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Andrei Halauneu on 04.10.2016.
 */
public class FileMovedToTrashPopUp extends AbstractPage {
    private static final String FILE_MOVED_TO_TRASH_LOCATOR = "//div[contains(text(),'Файл «%s%s» удален в')]";

    public FileMovedToTrashPopUp(WebDriver browser) {
        super(browser);
    }

    public String getFileToTrashMessage(String filename, String extension) {
        PageFactory.initElements(browser, FileMovedToTrashPopUp.class);
        waitForElementVisible(By.xpath(String.format(FILE_MOVED_TO_TRASH_LOCATOR, filename, extension)));
        return browser.findElement(By.xpath(
                String.format(FILE_MOVED_TO_TRASH_LOCATOR, filename, extension))).getText();
    }
}

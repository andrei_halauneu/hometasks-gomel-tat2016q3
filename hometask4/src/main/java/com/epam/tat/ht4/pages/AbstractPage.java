package com.epam.tat.ht4.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.tat.ht4.utils.DriverTimeouts.EXPLICIT_SECONDS;


/**
 * Created by Andrei Halauneu on 30.09.2016.
 */
public class AbstractPage {
    protected WebDriver browser;

    public AbstractPage(WebDriver browser) {
        this.browser = browser;
    }

    public void waitForElementVisible(By locator) {
        WebDriverWait wait = new WebDriverWait(browser, EXPLICIT_SECONDS.getValue());
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForInvisibility(By locator) {
        WebDriverWait wait = new WebDriverWait(browser, EXPLICIT_SECONDS.getValue());
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitForElementLoad(By locator) {
        WebDriverWait wait = new WebDriverWait(browser, EXPLICIT_SECONDS.getValue());
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
    }
}

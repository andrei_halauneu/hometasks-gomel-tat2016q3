package com.epam.tat.ht4.pages.ya;

import com.epam.tat.ht4.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.io.File;

/**
 * Created by Andrei Halauneu on 04.10.2016.
 */
public class MainPage extends AbstractPage {
    private static final By UPLOAD_FILE_LOCATOR = By.xpath("//input[@type='file']");
    private static final By CLOSE_AFTER_UPLOAD_BUTTON_LOCATOR = By.xpath("//a[@data-click-action='dialog.close']");
    private static final By TRASH_FOLDER_LOCATOR = By.xpath("//div[@title='Корзина']");
    private static final By DOWNLOAD_BUTTON_LOCATOR = By.xpath("//button[@data-click-action='resource.download']");
    private static final By DELETE_BUTTON = By.xpath("//button[@data-click-action='resource.delete']");
    private static final By RESTORE_BUTTON_LOCATOR = By.xpath("//button[@data-click-action='resource.restore']");
    private static final By PROGRESS_BAR_LOCATOR = By.xpath("//div[@class='b-progressbar__fill']");
    private static final By FILE_FOLDER_LOCATOR = By.xpath("//a[@href='/client/disk']");
    private static final String ATTACHMENT_LOCATION = "./src/main/resources/%s%s";
    private static final String FILE_NAME_LOCATOR = "//div[contains(text(),'%s')]";

    public MainPage(WebDriver browser) {
        super(browser);
    }

    public void selectFile(String filename) {
        waitForElementVisible(By.xpath(String.format(FILE_NAME_LOCATOR, filename)));
        browser.findElement(By.xpath(String.format(FILE_NAME_LOCATOR, filename))).click();
    }

    public void downloadFile(String filename) {
        selectFile(filename);
        waitForElementVisible(DOWNLOAD_BUTTON_LOCATOR);
        browser.findElement(DOWNLOAD_BUTTON_LOCATOR).click();
    }

    public void openFilesFolder() {
        browser.findElement(FILE_FOLDER_LOCATOR).click();
    }

    public void openTrashFolder() {
        waitForElementVisible(TRASH_FOLDER_LOCATOR);
        WebElement trash = browser.findElement(TRASH_FOLDER_LOCATOR);
        Actions action = new Actions(browser);
        action
                .doubleClick(trash)
                .build()
                .perform();
        waitForInvisibility(PROGRESS_BAR_LOCATOR);
    }

    public FileUploadedPopUp uploadFile(String filename, String extension) {
        waitForElementLoad(UPLOAD_FILE_LOCATOR);
        browser.findElement(UPLOAD_FILE_LOCATOR)
               .sendKeys(new File(String.format(ATTACHMENT_LOCATION, filename, extension))
               .getAbsolutePath());
        browser.findElement(CLOSE_AFTER_UPLOAD_BUTTON_LOCATOR).click();
        return new FileUploadedPopUp(browser);
    }

    public FileMovedToTrashPopUp moveFileToTrash(String filename) {
        Actions action = new Actions(browser);
        waitForElementVisible(TRASH_FOLDER_LOCATOR);
        WebElement trash = browser.findElement(TRASH_FOLDER_LOCATOR);
        WebElement file = browser.findElement(By.xpath(String.format(FILE_NAME_LOCATOR, filename)));
        action.clickAndHold(file);
        action.moveToElement(trash);
        action
                .release()
                .build()
                .perform();
        return new FileMovedToTrashPopUp(browser);
    }

    public FileRestoredPopUp restoreFile(String filename) {
        moveFileToTrash(filename);
        openTrashFolder();
        selectFile(filename);
        browser.findElement(RESTORE_BUTTON_LOCATOR).click();
        waitForInvisibility(PROGRESS_BAR_LOCATOR);
        browser.findElement(FILE_FOLDER_LOCATOR).click();
        return new FileRestoredPopUp(browser);
    }

    public FileDeletedPopUp deleteFile(String filename) {
        openTrashFolder();
        selectFile(filename);
        waitForElementVisible(DELETE_BUTTON);
        browser.findElement(DELETE_BUTTON).click();
        waitForInvisibility(PROGRESS_BAR_LOCATOR);
        browser.findElement(FILE_FOLDER_LOCATOR).click();
        return new FileDeletedPopUp(browser);
    }

    public MultiDeletedPopUp deleteTwoFiles(String fileOne, String fileTwo) {
        waitForElementVisible(By.xpath(String.format(FILE_NAME_LOCATOR, fileOne)));
        WebElement firstFile = browser.findElement(By.xpath(String.format(FILE_NAME_LOCATOR, fileOne)));
        WebElement secondFile = browser.findElement(By.xpath(String.format(FILE_NAME_LOCATOR, fileTwo)));
        WebElement trash = browser.findElement(TRASH_FOLDER_LOCATOR);
        Actions actions = new Actions(browser);
        actions.click(firstFile)
                .keyDown(Keys.CONTROL)
                .click(secondFile)
                .keyUp(Keys.CONTROL)
                .build()
                .perform();
        actions.clickAndHold(firstFile)
                .moveToElement(trash)
                .release()
                .build()
                .perform();
        return new MultiDeletedPopUp(browser);
    }
}

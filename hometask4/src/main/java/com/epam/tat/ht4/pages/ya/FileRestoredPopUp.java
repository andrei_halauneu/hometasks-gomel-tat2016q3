package com.epam.tat.ht4.pages.ya;

import com.epam.tat.ht4.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Andrei Halauneu on 04.10.2016.
 */
public class FileRestoredPopUp extends AbstractPage {
    private static final String FILE_RESTORED_LOCATOR = "//div[contains(text(),'Файл «%s%s» восстановлен')]";

    public FileRestoredPopUp(WebDriver browser) {
        super(browser);
    }

    public String getFileRestoreSuccessMessage(String filename, String extension) {
        PageFactory.initElements(browser, FileRestoredPopUp.class);
        waitForElementVisible(By.xpath(String.format(FILE_RESTORED_LOCATOR, filename, extension)));
        return browser.findElement(By.xpath(
                String.format(FILE_RESTORED_LOCATOR, filename, extension))).getText();
    }
}

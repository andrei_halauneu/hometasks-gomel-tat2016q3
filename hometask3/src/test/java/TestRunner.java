import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrei Halauneu on 30.09.2016.
 */
public class TestRunner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> files = Arrays.asList(
                "./src/test/suites/MailTests.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}

package com.epam.tat.ht3;

import com.epam.tat.ht3.mru.pages.ListOfLettersPage;
import com.epam.tat.ht3.mru.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;


/**
 * Created by Andrei Halauneu on 30.09.2016.
 */
public class SendAndSearchEmailTest extends BaseMailRuLoginTest {
    private String subject;
    private String textToBeSent;

    private String getRandomString() {
        return Long.toString(System.nanoTime());
    }

    @Test (description = "Composing and sending email")
    public void sendEmail() {
        subject = getRandomString();
        textToBeSent = getRandomString();
        new MainPage(driver).clickCompose().typeTo(EMAIL).typeSubject(subject).typeBody(textToBeSent).send();
    }

    @Test(description = "Checking sent email", dependsOnMethods = "sendEmail")
    public void checkSent() {
        ListOfLettersPage outbox = new MainPage(driver).openOutbox();
        Assert.assertEquals(outbox.getSubject(), subject);
    }

    @Test(description = "Checking inbox", dependsOnMethods = "sendEmail")
    public void checkInbox() {
        ListOfLettersPage inbox = new MainPage(driver).openInbox();
        Assert.assertEquals(inbox.getSubject(), subject);
        Assert.assertEquals(inbox.getFirstLine(), textToBeSent);
    }

    @Test(description = "Searching sent email by subject", dependsOnMethods = "sendEmail")
    public void searchEmailBySubject() {
        ListOfLettersPage searchResult = new MainPage(driver).makeSearch(subject);
        Assert.assertEquals(searchResult.getSubject(), subject);
        Assert.assertEquals(searchResult.getFirstLine(), textToBeSent);
    }
}

package com.epam.tat.ht3.mru.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Andrei Halauneu on 30.09.2016.
 */
public class ErrorPage {
    @FindBy(xpath = "//div[@class='b-login__errors']")
    private WebElement errorElement;

    public String getMessage() {
        return errorElement.getText();
    }
}

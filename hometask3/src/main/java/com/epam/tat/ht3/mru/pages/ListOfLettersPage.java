package com.epam.tat.ht3.mru.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Andrei Halauneu on 30.09.2016.
 */
public class ListOfLettersPage extends MainPage {
    private static final By SUBJECT_IN_LIST_LOCATOR = By.xpath("//span[contains(@class, 'subject')]//span[@title]");
    private static final By FIRSTLINE_IN_LIST_LOCATOR = By.xpath("//span[contains(@class, 'firstline')]//span[@title]");

    public ListOfLettersPage(WebDriver browser) {
        super(browser);
        waitForElement(FIRSTLINE_IN_LIST_LOCATOR);
    }

    public String getSubject() {
        return browser.findElement(SUBJECT_IN_LIST_LOCATOR).getText().trim();
    }

    public String getFirstLine() {
        return browser.findElement(FIRSTLINE_IN_LIST_LOCATOR).getText().trim();
    }
}

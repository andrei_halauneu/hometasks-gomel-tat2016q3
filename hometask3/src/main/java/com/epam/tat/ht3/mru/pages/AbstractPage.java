package com.epam.tat.ht3.mru.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.tat.ht3.utils.DriverTimeouts.EXPLICIT_SECONDS;


/**
 * Created by Andrei Halauneu on 30.09.2016.
 */
public class AbstractPage {
    protected WebDriver browser;

    public AbstractPage(WebDriver browser) {
        this.browser = browser;
    }

    protected WebElement waitForElement(By locator) {
        return new WebDriverWait(browser, EXPLICIT_SECONDS.getValue())
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}

package com.epam.tat.ht3;

import com.epam.tat.ht3.mru.pages.ListOfLettersPage;
import com.epam.tat.ht3.mru.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Andrei Halauneu on 30.09.2016.
 */
public class WriteEmailFromContactsTest extends BaseMailRuLoginTest {
    public static final String CONTACT_NAME = EMAIL.split("@")[0];

    private String subject;
    private String textToBeSent;

    private String getRandomStirng() {
        return Long.toString(System.nanoTime());
    }

    @Test (description = "Writing email to a contact")
    public void writeEmailToContact() {
        subject = getRandomStirng();
        textToBeSent = getRandomStirng();
        new MainPage(driver).openContacts().chooseContactByName(CONTACT_NAME).clickCompose().typeSubject(subject)
                .typeBody(textToBeSent).send();
    }

    @Test(description = "Checking inbox", dependsOnMethods = "writeEmailToContact")
    public void checkInbox() {
        ListOfLettersPage inbox = new MainPage(driver).openInbox();
        Assert.assertEquals(inbox.getSubject(), subject);
        Assert.assertEquals(inbox.getFirstLine(), textToBeSent);
    }
}

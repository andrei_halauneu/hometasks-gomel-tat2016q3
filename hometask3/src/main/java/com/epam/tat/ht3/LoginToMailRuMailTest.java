package com.epam.tat.ht3;

import com.epam.tat.ht3.mru.pages.ErrorPage;
import com.epam.tat.ht3.mru.pages.LoginPage;
import com.epam.tat.ht3.mru.pages.MainPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.epam.tat.ht3.BaseMailRuLoginTest.*;
import static com.epam.tat.ht3.utils.DriverTimeouts.IMPLICIT_WAIT;
import static com.epam.tat.ht3.utils.DriverTimeouts.PAGE_LOAD;


/**
 * Created by Andrei Halauneu on 30.09.2016.
 */
public class LoginToMailRuMailTest {
    public static final String INCORRECT_PASS = "incorrect";
    public static final String INVALID_CREDENTIALS = "Неверное имя пользователя или пароль. "
            + "Проверьте правильность введенных данных.";

    private WebDriver driver;

    @BeforeMethod
    private void setUpDriver() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT.getValue(), IMPLICIT_WAIT.getUnit());
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD.getValue(), PAGE_LOAD.getUnit());
        driver.manage().window().maximize();
    }

    @Test (description = "Loggin in with correct password")
    public void loginToMailWithCorrectPassword() {
        driver.get(SITE);
        MainPage home = new LoginPage(driver).login(LOGIN, CORRECT_PASS);
        Assert.assertEquals(home.getUserName(), EMAIL);
    }

    @Test (description = "Loggin in with incorrect password")
    public void loginToMailWithIncorrectPassword() {
        driver.get(SITE);
        ErrorPage errorPage = new LoginPage(driver).failedLogin(LOGIN, INCORRECT_PASS);
        Assert.assertEquals(errorPage.getMessage(), INVALID_CREDENTIALS, "Wrong password!");
    }

    @AfterMethod
    public void killDriver() {
        driver.quit();
    }
}

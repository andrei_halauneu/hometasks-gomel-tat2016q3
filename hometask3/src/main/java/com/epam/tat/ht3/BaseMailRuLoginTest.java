package com.epam.tat.ht3;

import com.epam.tat.ht3.mru.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import static com.epam.tat.ht3.utils.DriverTimeouts.IMPLICIT_WAIT;
import static com.epam.tat.ht3.utils.DriverTimeouts.PAGE_LOAD;


/**
 * Created by Andrei Halauneu on 30.09.2016.
 */
public class BaseMailRuLoginTest {
    public static final String LOGIN = "tat-hwrk";
    public static final String EMAIL = LOGIN + "@mail.ru";
    public static final String CORRECT_PASS = "qwe123asd";
    public static final String SITE = "https://mail.ru/";

    protected WebDriver driver;

    @BeforeClass
    public void setUpDriver() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT.getValue(), IMPLICIT_WAIT.getUnit());
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD.getValue(), PAGE_LOAD.getUnit());
        driver.manage().window().maximize();
    }

    @BeforeClass(dependsOnMethods = "setUpDriver")
    public void loginToMail() {
        driver.get(SITE);
        LoginPage loginPage = new LoginPage(driver);
        Assert.assertEquals(loginPage.login(EMAIL, CORRECT_PASS).getUserName(), EMAIL);
    }

    @AfterClass
    public void killDriver() {
        driver.quit();
    }
}

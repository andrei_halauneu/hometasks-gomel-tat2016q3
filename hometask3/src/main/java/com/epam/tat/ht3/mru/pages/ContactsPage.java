package com.epam.tat.ht3.mru.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Andrei Halauneu on 30.09.2016.
 */
public class ContactsPage extends MainPage {

    private static final String CONTACT_PATTERN_LOCATOR =
            "//span[contains(text(), '%s')]//preceding::label[contains(@class, 'checkbox')]";

    public ContactsPage(WebDriver browser) {
        super(browser);
    }

    public ContactsPage chooseContactByName(String name) {
        browser.findElement(By.xpath(String.format(CONTACT_PATTERN_LOCATOR, name))).click();
        return this;
    }
}

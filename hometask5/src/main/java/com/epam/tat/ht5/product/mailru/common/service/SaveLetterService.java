package com.epam.tat.ht5.product.mailru.common.service;


import com.epam.tat.ht5.product.mailru.common.bo.MailLetter;
import com.epam.tat.ht5.product.mailru.common.screen.MailMainPage;
import com.epam.tat.ht5.product.mailru.common.screen.MailTopPage;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class SaveLetterService {

    public boolean isMailSavedInDrafts(MailLetter mailLetter) {
        MailTopPage mailTopPage = new MailTopPage();
        MailMainPage mailMainPage = new MailMainPage();
        mailTopPage.openToWriteMail();
        mailMainPage
                .setMailAddress(mailLetter.getAddress())
                .setMailSubject(mailLetter.getSubject())
                .setMailBodyText(mailLetter.getText());
        mailTopPage.saveInDraftMail();
        return mailMainPage.isMailSuccessSaveInDraft();
    }
}

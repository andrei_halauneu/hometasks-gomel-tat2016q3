package com.epam.tat.ht5.product.yandex.common.service;

import com.epam.tat.ht5.framework.util.FileDownloadValidator;
import com.epam.tat.ht5.product.yandex.common.exception.YandexUploadFileException;
import com.epam.tat.ht5.product.yandex.common.page.YandexHeaderPage;
import com.epam.tat.ht5.product.yandex.common.page.YandexMainPage;
import com.epam.tat.ht5.product.yandex.common.page.YandexRightFramePage;
import org.openqa.selenium.NoSuchElementException;

import java.io.IOException;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexUploadDownloadService {
    private FileDownloadValidator validator = new FileDownloadValidator();

    public void uploadFile(String fileName, String filePath) {
        YandexHeaderPage yandexHeaderPage = new YandexHeaderPage();
        yandexHeaderPage
                .uploadFile(fileName, filePath)
                .waitForUploadingFile(fileName)
                .closeDialogUploadingFile();
    }

    public void checkFileUploaded(String fileName) {
        YandexMainPage yaDiskContentPage = new YandexMainPage();
        try {
            yaDiskContentPage.findFile(fileName);
        } catch (NoSuchElementException e) {
            throw new YandexUploadFileException("File not found on yandex disk");
        }
    }

    public void downloadFile(String fileName) {
        YandexMainPage yaDiskContentPage = new YandexMainPage();
        YandexRightFramePage yaDiskRightMenuPage = new YandexRightFramePage();
        yaDiskContentPage.findFile(fileName);
        yaDiskContentPage.pickFile();
        yaDiskRightMenuPage.downloadFile();
        validator.waitForFile(fileName);
    }

    public void checkFileContent(String fileName) throws IOException {
        Boolean expectedResult = true;
        if (!expectedResult == validator.isContentFileSame(fileName)) {
            throw new YandexUploadFileException("Different files!");
        }
    }

    public void deleteDownloadDirectory() throws IOException {
        validator.deleteDownloadDirectory();
    }
}

package com.epam.tat.ht5.product.yandex.common.exception;


import com.epam.tat.ht5.framework.exception.CommonTestRuntimeException;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexFailedOperationException extends CommonTestRuntimeException {
    public YandexFailedOperationException(String message) {
        super(message);
    }
}

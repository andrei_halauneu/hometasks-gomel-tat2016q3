package com.epam.tat.ht5.framework.listener;

import com.epam.tat.ht5.framework.ui.browser.Browser;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * Created by Andrei Halauneu on 29.10.2016.
 */
public class TestListener extends TestListenerAdapter {

    @Override
    public void onTestSuccess(ITestResult tr) {
        Browser.getBrowser().close();
    }

    @Override
    public void onTestFailure(ITestResult tr) {
        Browser.getBrowser().close();
    }
}

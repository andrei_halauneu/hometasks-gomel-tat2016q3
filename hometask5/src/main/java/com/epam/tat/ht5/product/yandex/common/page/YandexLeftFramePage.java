package com.epam.tat.ht5.product.yandex.common.page;

import com.epam.tat.ht5.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexLeftFramePage {
    private static final By FILE_SECTION_LOCATOR = By.xpath("//a[@href='/client/disk']");
    private static final By FILE_SECTION_ACTIVE_LOCATOR
            = By.xpath("//a[@class='navigation__item navigation__item_current' and @href='/client/disk']");

    private Element filesLink = new Element(FILE_SECTION_LOCATOR);
    private Element filesIsActive = new Element(FILE_SECTION_ACTIVE_LOCATOR);

    public void enterToFilesSection() {
        filesLink.waitForAppear();
        filesLink.click();
        filesIsActive.waitForAppear();
    }

}

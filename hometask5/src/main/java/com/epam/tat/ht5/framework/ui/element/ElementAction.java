package com.epam.tat.ht5.framework.ui.element;

import com.epam.tat.ht5.framework.ui.browser.Browser;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by Andrei Halauneu on 29.10.2016.
 */
public class ElementAction {
    private Actions action = Browser.getBrowser().getActions();

    public Actions actOnElement() {
        return action;
    }

    public ElementAction click (Element element) {
        actOnElement().click();
        return this;
    }

    public ElementAction doubleClick(Element element) {
        actOnElement().doubleClick(element.getWrappedWebElement());
        return this;
    }

    public ElementAction buildAndPerform() {
        actOnElement().build().perform();
        return this;
    }

    public ElementAction release() {
        actOnElement().release();
        return this;
    }

    public ElementAction clickAndHold(Element element) {
        actOnElement().clickAndHold(element.getWrappedWebElement());
        return this;
    }

    public ElementAction moveToElement(Element element) {
        actOnElement().moveToElement(element.getWrappedWebElement());
        return this;
    }

    public ElementAction keyDown (Keys keys) {
        actOnElement().keyDown(keys);
        return this;
    }

    public ElementAction keyUp(Keys keys) {
        actOnElement().keyUp(keys);
        return this;
    }
}

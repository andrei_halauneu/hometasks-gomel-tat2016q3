package com.epam.tat.ht5.product.mailru.common.screen;

import com.epam.tat.ht5.framework.ui.element.Element;
import com.epam.tat.ht5.product.mailru.common.MailGlobalParameters;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailMainPage {
    private static final By ADDRESS_SEND_LOCATOR
            = By.xpath("//textarea[@class='js-input compose__labels__input' and @data-original-name='To']");
    private static final By SUBJECT_LOCATOR = By.xpath("//input[@class='compose__header__field']");
    private static final By TEXT_MAIL_LOCATOR = By.xpath("//html/body");
    private static final By IFRAME_SENT_TEXT_LOCATOR
            = By.xpath("//iframe[contains(@id,'composeEditor_ifr')]");
    private static final By ATTACH_BUTTON_LOCATOR = By.xpath("//input[@name='Filedata']");
    private static final String CHECKBOX_MAIL_WITH_SUBJECT
            = "//a[@data-subject='%s' and not(ancestor::div[contains(@style,'display:none')]) "
            + "and not(ancestor::div[contains(@style,'display: none')])]"
            + "//div[@class='js-item-checkbox b-datalist__item__cbx']";
    private static final String SUBJECT_MAIL = "//a[@data-subject='%s' "
            + "and not(ancestor::div[contains(@style,'display:none')]) "
            + "and not(ancestor::div[contains(@style,'display: none')])]";
    private static final By ATTACH_SUCCESS_UPLOADED_LOCATOR
            = By.xpath("//i[@class='js-ok upload__file__ico upload__file__ico_ok']");
    private static final By SENT_MESSAGE_SUCCESS_LOCATOR = By.xpath("(//div[@class='message-sent__title'])");
    private static final By FIND_MAIL_WITHOUT_SUBJECT_LOCATOR = By.xpath("//div[contains(text(),'Без темы') "
            + "and not(ancestor::div[contains(@style,'display:none')]) "
            + "and not(ancestor::div[contains(@style,'display: none')])]");
    private static final By SUCCESS_SAVE_IN_DRAFT_LOCATOR = By.xpath("//div[@class='b-toolbar__message']");

    private Element addressInput = new Element(ADDRESS_SEND_LOCATOR);
    private Element subjectInput = new Element(SUBJECT_LOCATOR);
    private Element textMailInput = new Element(TEXT_MAIL_LOCATOR);
    private Element iFrameText = new Element(IFRAME_SENT_TEXT_LOCATOR);
    private Element attachFileButton = new Element(ATTACH_BUTTON_LOCATOR);
    private Element fileUploaded = new Element(ATTACH_SUCCESS_UPLOADED_LOCATOR);
    private Element sentSuccessMessage = new Element(SENT_MESSAGE_SUCCESS_LOCATOR);
    private Element mailWithoutSubject = new Element(FIND_MAIL_WITHOUT_SUBJECT_LOCATOR);
    private Element saveInDraftsSuccess = new Element(SUCCESS_SAVE_IN_DRAFT_LOCATOR);

    public MailMainPage setMailAddress(String address) {
        addressInput.waitForAppear();
        addressInput.typeValue(address);
        return this;
    }

    public MailMainPage setMailSubject(String subject) {
        subjectInput.waitForAppear();
        subjectInput.typeValue(subject);
        return this;
    }

    public MailMainPage setMailBodyText(String text) {
        textMailInput.waitForAppear();
        iFrameText.switchToFrame();
        textMailInput.typeValue(text);
        iFrameText.switchToDefaultFrame();
        return this;
    }

    public MailMainPage attachFileToMail() {
        attachFileButton.sendFile(MailGlobalParameters.LOCATION_FILE);
        fileUploaded.waitForAppear();
        return this;
    }

    public MailMainPage clickNeededMailCheckbox(String subject) {
        new Element(By.xpath(String.format(CHECKBOX_MAIL_WITH_SUBJECT, subject))).waitForAppear();
        Element checkboxForNeededMail = new Element(By.xpath(String.format(CHECKBOX_MAIL_WITH_SUBJECT, subject)));
        checkboxForNeededMail.click();
        return this;
    }

    public boolean findNeededMailBySubject(String subject) {
        new Element(By.xpath(String.format(SUBJECT_MAIL, subject))).waitForAppear();
        Element mailNeeded = new Element(By.xpath(String.format(SUBJECT_MAIL, subject)));
        return mailNeeded.isVisible();
    }

    public boolean findNeededMailWithoutSubject() {
        mailWithoutSubject.waitForAppear();
        return mailWithoutSubject.isVisible();
    }

    public void waitDisappearNeededMailBySubject(String subject) throws TimeoutException {
        new Element(By.xpath(String.format(SUBJECT_MAIL, subject))).waitForDisappear();
    }

    public boolean isMailSuccessSaveInDraft() {
        saveInDraftsSuccess.waitForAppear();
        return saveInDraftsSuccess.isVisible();
    }

    public String getSuccessSentMessage() {
        sentSuccessMessage.waitForAppear();
        return sentSuccessMessage.getText();
    }
}

package com.epam.tat.ht5.product.yandex.common.page;

import com.epam.tat.ht5.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexRightFramePage {

    private static final By BUTTON_DOWNLOAD_LOCATOR = By.xpath("//button[@data-click-action='resource.download']");
    private static final By BUTTON_PERMANENTLY_DELETE_LOCATOR =
            By.xpath("//button[@data-click-action='resource.delete']");
    private static final By BUTTON_RESTORE_LOCATOR = By.xpath("//button[@data-click-action='resource.restore']");

    private Element downloadButton = new Element(BUTTON_DOWNLOAD_LOCATOR);
    private Element deleteButton = new Element(BUTTON_PERMANENTLY_DELETE_LOCATOR);
    private Element restoreButton = new Element(BUTTON_RESTORE_LOCATOR);

    public void downloadFile() {
        downloadButton.waitForAppear();
        downloadButton.click();
    }

    public void deleteFile() {
        deleteButton.waitForAppear();
        deleteButton.click();
    }

    public void restoreFile() {
        restoreButton.waitForAppear();
        restoreButton.click();
    }

}



package com.epam.tat.ht5.product.mailru.common.screen;

import com.epam.tat.ht5.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailMenuPage {
    private static final By INBOX_MAIL_SECTION_BUTTON_LOCATOR
            = By.xpath("//div[@data-mnemo='nav-folders']//a[@href='/messages/inbox/']");
    private static final By SENT_MAIL_SECTION_BUTTON_LOCATOR
            = By.xpath("//div[@data-mnemo='nav-folders']//a[@href='/messages/sent/']");
    private static final By DRAFTS_MAIL_SECTION_BUTTON_LOCATOR
            = By.xpath("//div[@data-mnemo='nav-folders']//a[@href='/messages/drafts/']");
    private static final By TRASH_MAIL_SECTION_BUTTON_LOCATOR
            = By.xpath("//div[@data-mnemo='nav-folders']//a[@href='/messages/trash/']");
    private static final By CLEAN_TRASH_BUTTON_LOCATOR
            = By.xpath("//button[@class='btn btn_main btn_stylish js-clearButton']");
    private static final By CHOICE_FRAME_TRASH_CLEAN_LOCATOR
            = By.xpath("//span[@class='b-nav__link__text js-clear-text']");
    private static final By IN_TRASH_SECTION_ACTIVE_LOCATOR
            = By.xpath("//div[contains(@class,'b-nav__item_active')]//a[@href='/messages/trash/']");
    private static final By IN_SENT_SECTION_ACTIVE_LOCATOR
            = By.xpath("//div[contains(@class,'b-nav__item_active')]//a[@href='/messages/sent/']");
    private static final By IN_INBOX_SECTION_ACTIVE_LOCATOR
            = By.xpath("//div[contains(@class,'b-nav__item_active')]//a[@href='/messages/inbox/']");
    private static final By IN_DRAFTS_SECTION_ACTIVE_LOCATOR
            = By.xpath("//div[contains(@class,'b-nav__item_active')]//a[@href='/messages/drafts/']");

    private Element inboxMailLink = new Element(INBOX_MAIL_SECTION_BUTTON_LOCATOR);
    private Element sentMailLink = new Element(SENT_MAIL_SECTION_BUTTON_LOCATOR);
    private Element draftsMailLink = new Element(DRAFTS_MAIL_SECTION_BUTTON_LOCATOR);
    private Element trashMailLink = new Element(TRASH_MAIL_SECTION_BUTTON_LOCATOR);
    private Element cleanAllMailFrame = new Element(CHOICE_FRAME_TRASH_CLEAN_LOCATOR);
    private Element cleanExecute = new Element(CLEAN_TRASH_BUTTON_LOCATOR);
    private Element trashActiveSection = new Element(IN_TRASH_SECTION_ACTIVE_LOCATOR);
    private Element sentActiveSection = new Element(IN_SENT_SECTION_ACTIVE_LOCATOR);
    private Element inboxActiveSection = new Element(IN_INBOX_SECTION_ACTIVE_LOCATOR);
    private Element draftsActiveSection = new Element(IN_DRAFTS_SECTION_ACTIVE_LOCATOR);

    public void enterToInboxMail() {
        inboxMailLink.click();
        inboxActiveSection.waitForAppear();
    }

    public void enterToSentMail() {
        sentMailLink.click();
        sentActiveSection.waitForAppear();
    }

    public void enterToDraftsMail() {
        draftsMailLink.click();
        draftsActiveSection.waitForAppear();
    }

    public void enterToTrashMail() {
        trashMailLink.click();
        trashActiveSection.waitForAppear();
    }

    public void cleanAllMailInTrash() {
        cleanAllMailFrame.click();
        cleanExecute.click();
    }

}

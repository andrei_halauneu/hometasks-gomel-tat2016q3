package com.epam.tat.ht5.test.mailru;


import com.epam.tat.ht5.product.mailru.account.bo.MailAccountFactory;
import com.epam.tat.ht5.product.mailru.account.service.MailLoginService;
import org.testng.annotations.Test;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class MailRuLoginTest {
    private static final String FAILED_LOGIN_MESSAGE
            = "Неверное имя пользователя или пароль. Проверьте правильность введенных данных.";
    private MailLoginService mailLoginService = new MailLoginService();

    @Test(description = "Logging to 'mail.ru'account from login page")
    public void loginToMailRuFromLoginPage() {
        mailLoginService.login(MailAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Expecting failed login error message")
    public void checkFailedLoginErrorMessage() {
        mailLoginService.unsafeLogin(MailAccountFactory.createWrongDataAccount());
        mailLoginService.checkErrorMessage(FAILED_LOGIN_MESSAGE);
    }
}


package com.epam.tat.ht5.product.mailru.common.bo;


import com.epam.tat.ht5.framework.util.Randoms;
import com.epam.tat.ht5.product.mailru.common.MailGlobalParameters;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailLetterFactory {

    public static MailLetter createMailWithAllFields() {
        MailLetter mailLetter = new MailLetter();
        mailLetter.setAddress(MailGlobalParameters.DEFAULT_ACCOUNT_EMAIL);
        mailLetter.setSubject(Randoms.randomAlphabetic());
        mailLetter.setText(Randoms.randomAlphabetic());
        return mailLetter;

    }

    public static MailLetter createMailWithoutAddress() {
        MailLetter mailLetter = new MailLetter();
        mailLetter.setSubject(Randoms.randomAlphabetic());
        mailLetter.setText(Randoms.randomAlphabetic());
        return mailLetter;
    }

    public static MailLetter createMailWithoutSubject() {
        MailLetter mailLetter = new MailLetter();
        mailLetter.setAddress(MailGlobalParameters.DEFAULT_ACCOUNT_EMAIL);
        mailLetter.setText(Randoms.randomAlphabetic());
        return mailLetter;
    }
}

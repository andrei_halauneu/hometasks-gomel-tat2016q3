package com.epam.tat.ht5.test.mailru;


import com.epam.tat.ht5.product.mailru.account.bo.MailAccountFactory;
import com.epam.tat.ht5.product.mailru.account.service.MailLoginService;
import com.epam.tat.ht5.product.mailru.common.bo.MailLetter;
import com.epam.tat.ht5.product.mailru.common.bo.MailLetterFactory;
import com.epam.tat.ht5.product.mailru.common.service.FindLetterService;
import com.epam.tat.ht5.product.mailru.common.service.SendLetterService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class SendMailWithoutSubjectTest {
    private static final String SUCCESS_MESSAGE = "Ваше письмо отправлено. Перейти во Входящие";
    private MailLoginService mailLoginService = new MailLoginService();
    private SendLetterService sendLetterService = new SendLetterService();

    @BeforeMethod
    public void init() {
        mailLoginService.login(MailAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Sending a letter without subject and redirecting with success message")
    public void sentMailWithoutSubjectTest() throws InterruptedException {
        MailLetter mailLetter = MailLetterFactory.createMailWithoutSubject();
        sendLetterService.sendMailWithoutSubject(mailLetter);
        sendLetterService.checkSuccessSentMailMessage(SUCCESS_MESSAGE);
    }
}

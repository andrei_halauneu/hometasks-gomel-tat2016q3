package com.epam.tat.ht5.product.yandex.account.bo;


import com.epam.tat.ht5.framework.util.Randoms;
import com.epam.tat.ht5.product.yandex.common.YandexGlobalParameters;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexAccountFactory {

    public static YandexAccount createCorrectDataAccount() {
        YandexAccount yandexAccount = new YandexAccount();
        yandexAccount.setUsername(YandexGlobalParameters.DEFAULT_ACCOUNT_USERNAME);
        yandexAccount.setPassword(YandexGlobalParameters.DEFAULT_ACCOUNT_PASSWORD);
        return yandexAccount;
    }

    public static YandexAccount createWrongDataAccount() {
        YandexAccount yandexAccount = new YandexAccount();
        yandexAccount.setPassword(Randoms.randomAlphabetic());
        yandexAccount.setUsername(Randoms.randomAlphabetic());
        return yandexAccount;
    }
}

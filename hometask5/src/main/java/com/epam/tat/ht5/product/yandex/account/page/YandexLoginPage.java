package com.epam.tat.ht5.product.yandex.account.page;


import com.epam.tat.ht5.framework.ui.browser.Browser;
import com.epam.tat.ht5.framework.ui.element.Element;
import com.epam.tat.ht5.product.yandex.common.YandexGlobalParameters;
import org.openqa.selenium.By;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexLoginPage {
    private static final String URL = YandexGlobalParameters.DEFAULT_PRODUCT_URL;
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='password']");
    private static final By BUTTON_LOGIN_LOCATOR = By.xpath("//div[@class='nb-input-group']//button");
    private static final By LOGIN_FAILED_LOCATOR = By.xpath("//label["
            + "@class='nb-input _nb-complex-input nb-form-auth__input _init _nb-is-wrong']");

    private Element loginInput = new Element(LOGIN_INPUT_LOCATOR);
    private Element passwordInput = new Element(PASSWORD_INPUT_LOCATOR);
    private Element buttonLogin = new Element(BUTTON_LOGIN_LOCATOR);
    private Element errorLogin = new Element(LOGIN_FAILED_LOCATOR);

    public YandexLoginPage open() {
        Browser.getBrowser().open(URL);
        return this;
    }

    public YandexLoginPage setLogin(String login) {
        loginInput.typeValue(login);
        return this;
    }

    public YandexLoginPage setPassword(String password) {
        passwordInput.typeValue(password);
        return this;
    }

    public void signIn() {
        buttonLogin.click();
    }

    public boolean checkFailedLogin() {
        errorLogin.waitForAppear();
        return errorLogin.isVisible();
    }

}

package com.epam.tat.ht5.framework.runner;

import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrei Halauneu on 29.10.2016.
 */
public class YandexRunner {

    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> files = Arrays.asList("test-suites/yandex.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}

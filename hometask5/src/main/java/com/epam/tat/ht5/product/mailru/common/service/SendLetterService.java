package com.epam.tat.ht5.product.mailru.common.service;


import com.epam.tat.ht5.framework.ui.element.ElementAlert;
import com.epam.tat.ht5.product.mailru.common.bo.MailLetter;
import com.epam.tat.ht5.product.mailru.common.exception.MailSendException;
import com.epam.tat.ht5.product.mailru.common.screen.MailMainPage;
import com.epam.tat.ht5.product.mailru.common.screen.MailTopPage;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class SendLetterService {
    MailTopPage mailTopPage = new MailTopPage();
    MailMainPage mailMainPage = new MailMainPage();

    public void sendMailWithFields(MailLetter mailLetter) {
        mailTopPage.openToWriteMail();
        mailMainPage
                .setMailAddress(mailLetter.getAddress())
                .setMailSubject(mailLetter.getSubject())
                .setMailBodyText(mailLetter.getText())
                .attachFileToMail();
        mailTopPage.sendMail();
    }

    public void checkSuccessSentMailMessage(String expectedMessage) {
        String actualMessage = new MailMainPage().getSuccessSentMessage();
        if (!actualMessage.equals(expectedMessage)) {
            throw new MailSendException(String.format(
                    "Expected message is '%s' but actual is '%s'", expectedMessage, actualMessage));
        }
    }

    public String sendMailWithoutAddress() {
        mailTopPage.openToWriteMail();
        mailTopPage.sendMail();
        ElementAlert elementAlert = new ElementAlert();
        String message = elementAlert.getAlertText();
        elementAlert.acceptAlert();
        return message;
    }

    public void sendMailWithoutSubject(MailLetter mailLetter) {
        mailTopPage.openToWriteMail();
        mailMainPage.setMailAddress(mailLetter.getAddress()).setMailBodyText(mailLetter.getText());
        mailTopPage.sendMail();
    }

}

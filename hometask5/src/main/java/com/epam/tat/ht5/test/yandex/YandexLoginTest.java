package com.epam.tat.ht5.test.yandex;


import com.epam.tat.ht5.product.yandex.account.bo.YandexAccountFactory;
import com.epam.tat.ht5.product.yandex.account.service.YandexLoginService;
import org.testng.annotations.Test;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexLoginTest {
    private YandexLoginService yandexLoginService = new YandexLoginService();

    @Test(description = "Login to 'disk.yandex.ru' from login page")
    public void loginToYaDiskFromLoginPage() {
        yandexLoginService.login(YandexAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Check failed authentication error message")
    public void checkFailedLoginErrorMessage() {
        yandexLoginService.unsafeLogin(YandexAccountFactory.createWrongDataAccount());
        boolean expectedResult = true;
        yandexLoginService.checkFailedLogin(expectedResult);
    }
}

package com.epam.tat.ht5.product.mailru.account.exception;


import com.epam.tat.ht5.framework.exception.CommonTestRuntimeException;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailLoginException extends CommonTestRuntimeException {

    public MailLoginException(String message) {
        super(message);
    }
}

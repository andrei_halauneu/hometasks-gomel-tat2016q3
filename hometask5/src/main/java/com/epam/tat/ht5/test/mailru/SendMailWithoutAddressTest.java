package com.epam.tat.ht5.test.mailru;


import com.epam.tat.ht5.product.mailru.account.bo.MailAccountFactory;
import com.epam.tat.ht5.product.mailru.account.service.MailLoginService;
import com.epam.tat.ht5.product.mailru.common.service.SendLetterService;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class SendMailWithoutAddressTest {
    private static final String SUCCESS_MESSAGE = "Не указан адрес получателя";
    private MailLoginService mailLoginService = new MailLoginService();
    private SendLetterService sendLetterService = new SendLetterService();

    @BeforeMethod
    public void init() {
        mailLoginService.login(MailAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Sending a letter without address and getting alert")
    public void sentMailWithoutAddressTest() throws InterruptedException {
        String actualResult = sendLetterService.sendMailWithoutAddress();
        Assert.assertEquals(actualResult, SUCCESS_MESSAGE, "sentMailWithoutAddressTest error");
    }
}


package com.epam.tat.ht5.product.yandex.account.bo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexAccount {
    @Getter
    @Setter
    private String username;
    @Getter
    @Setter
    private String password;

}

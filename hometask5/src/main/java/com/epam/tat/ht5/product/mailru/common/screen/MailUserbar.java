package com.epam.tat.ht5.product.mailru.common.screen;

import com.epam.tat.ht5.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailUserbar {
    private static final By LOGIN_SUCCESS_LOCATOR = By.xpath("//i[@id='PH_user-email']");
    Element loginBar = new Element(LOGIN_SUCCESS_LOCATOR);

    public boolean isLoggedIn() {
        return loginBar.isVisible();
    }

}

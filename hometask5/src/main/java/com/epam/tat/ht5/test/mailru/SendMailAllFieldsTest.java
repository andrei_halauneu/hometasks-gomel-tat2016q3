package com.epam.tat.ht5.test.mailru;


import com.epam.tat.ht5.product.mailru.account.bo.MailAccountFactory;
import com.epam.tat.ht5.product.mailru.account.service.MailLoginService;
import com.epam.tat.ht5.product.mailru.common.bo.MailLetter;
import com.epam.tat.ht5.product.mailru.common.bo.MailLetterFactory;
import com.epam.tat.ht5.product.mailru.common.service.FindLetterService;
import com.epam.tat.ht5.product.mailru.common.service.SendLetterService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class SendMailAllFieldsTest {
    private static final String SUCCESS_MESSAGE = "Ваше письмо отправлено. Перейти во Входящие";
    private MailLoginService mailLoginService = new MailLoginService();
    private SendLetterService sendLetterService = new SendLetterService();
    private FindLetterService findLetterService = new FindLetterService();
    private String mailSubject;

    @BeforeMethod
    public void init() {
        mailLoginService.login(MailAccountFactory.createCorrectDataAccount());
    }

    @Test(description = "Sending a letter with all expected field filled "
            + "redirecting if success")
    public void sendMailAllFields() throws InterruptedException {
        MailLetter mailLetter = MailLetterFactory.createMailWithAllFields();
        sendLetterService.sendMailWithFields(mailLetter);
        mailSubject = mailLetter.getSubject();
        sendLetterService.checkSuccessSentMailMessage(SUCCESS_MESSAGE);
    }
}


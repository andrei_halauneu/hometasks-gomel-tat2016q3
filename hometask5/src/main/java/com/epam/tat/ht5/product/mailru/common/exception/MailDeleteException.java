package com.epam.tat.ht5.product.mailru.common.exception;


import com.epam.tat.ht5.framework.exception.CommonTestRuntimeException;

/**
 * Created by Andrei Halauneu on 30.10.2016.
 */
public class MailDeleteException extends CommonTestRuntimeException {

    public MailDeleteException(String message) {
        super(message);
    }
}

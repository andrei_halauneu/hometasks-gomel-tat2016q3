package com.epam.tat.ht5.product.yandex.common.page;

import com.epam.tat.ht5.framework.ui.element.Element;
import org.openqa.selenium.By;

/**
 * Created by Andrei Halauneu on 31.10.2016.
 */
public class YandexHeaderPage {
    private static final By LOGIN_SUCCESS_LOCATOR = By.xpath("//span[@class='header__username']");
    private static final By UPLOAD_FILE_LOCATOR = By.xpath("//input[@type='file']");
    private static final By CLOSE_UPLOAD_DIALOG_LOCATOR
            = By.xpath("//a[@data-click-action='dialog.close']");
    private static final String UPLOAD_FILE_COMPLETE_LOCATOR
            = "//div[starts-with(@class,'ns-view-itemUploadDone')]//div[@title='%s.txt']";
    private Element loginBar = new Element(LOGIN_SUCCESS_LOCATOR);
    private Element attachFileButton = new Element(UPLOAD_FILE_LOCATOR);
    private Element closeUploadDialog = new Element(CLOSE_UPLOAD_DIALOG_LOCATOR);

    public boolean isLoggedIn() {
        loginBar.waitForAppear();
        return loginBar.isVisible();
    }

    public YandexHeaderPage uploadFile(String fileName, String filePath) {
        attachFileButton.waitForAppear();
        attachFileButton.sendFile(String.format(filePath, fileName));
        return this;
    }

    public YandexHeaderPage closeDialogUploadingFile() {
        closeUploadDialog.waitForAppear();
        closeUploadDialog.click();
        return this;
    }

    public YandexHeaderPage waitForUploadingFile(String fileName) {
        Element uploadFileStage = new Element(By.xpath(String.format(UPLOAD_FILE_COMPLETE_LOCATOR, fileName)));
        uploadFileStage.waitForAppear();
        return this;
    }

}

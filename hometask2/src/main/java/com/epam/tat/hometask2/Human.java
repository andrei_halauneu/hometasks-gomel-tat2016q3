package com.epam.tat.hometask2;

public abstract class Human {

    public abstract Mood getMood();
}

package com.epam.tat.hometask2;

public enum Mood {
    EXCELLENT, GOOD, NEUTRAL, BAD, HORRIBLE, I_HATE_THEM_ALL
}

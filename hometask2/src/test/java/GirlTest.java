import com.epam.tat.hometask2.Boy;
import com.epam.tat.hometask2.Girl;
import com.epam.tat.hometask2.Mood;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Andrei Halauneu on 23.09.2016.
 */
@Listeners({TestListener.class})
public class GirlTest {

    private Boy boyfriend;
    private Girl girl;

    @BeforeMethod(description = "Preparing objects")
    public void setUp() {
        boyfriend = new Boy();
        girl = new Girl();
    }

    @Test(description = "If boyfriend is rich and girl is pretty, then mood is excellent")
    public void testMoodExcellent() throws Exception {
        girl.setPretty(true);
        boyfriend.setWealth(1000000D);
        girl.setBoyFriend(boyfriend);
        Mood actualResult = girl.getMood();
        Assert.assertEquals(actualResult, Mood.EXCELLENT, "The mood isn't excellent");
    }

    @Test(description = "If boyfriend has one million of money, then he is rich", groups = "boyfriend")
    public void isBoyfriendRichTest() {
        girl.setPretty(true);;
        boyfriend.setWealth(1000000D);
        girl.setBoyFriend(boyfriend);
        boolean isBoyfriendRich = girl.isBoyfriendRich();
        assertThat("Boyfriend is not rich", isBoyfriendRich, is(true));
    }

    @Test(description = "If boyfriend has one million of money and girl is pretty, then the boyfriend will buy " +
            "new shoes", groups = "boyfriend")
    public void isBoyFriendWillBuyNewShoesTest() {
        boyfriend.setWealth(1000000D);
        Girl girl = new Girl(true, true, boyfriend);
        boolean isBoyFriendWillBuyNewShoes = girl.isBoyFriendWillBuyNewShoes();
        Assert.assertTrue(isBoyFriendWillBuyNewShoes, "Boyfriend won't buy new shoes");
    }

    @Test(description = "If girl is not pretty and her slim friend got a few kilos, then the slim friend became fat")
    public void isSlimFriendBecameFatTest() {
        girl.setPretty(false);
        girl.setSlimFriendGotAFewKilos(true);
        boolean isSlimFriendBecameFat = girl.isSlimFriendBecameFat();
        Assert.assertTrue(isSlimFriendBecameFat, "Slim friend not became fat");
    }

}


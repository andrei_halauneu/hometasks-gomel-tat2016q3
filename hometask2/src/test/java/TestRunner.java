import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrei Halauneu on 23.09.2016.
 */
public class TestRunner {
    public static void main(String[] args) {
        final String SUITE_PATH = "./src/test/resources/suites/";
        final String TEST_PATH = "boy.xml";
        TestNG testNG = new TestNG();
        List<String> files = Arrays.asList(SUITE_PATH + TEST_PATH);
        testNG.setTestSuites(files);
        testNG.run();
    }
}

